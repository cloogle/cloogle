# Cloogle

This is the core system of Cloogle, a search engine for [Clean][].
The source code of the web frontend is in
[cloogle-web](https://gitlab.com/cloogle/cloogle-web).

## Structure
The database is a data structure that can be stored on the disk using generic
JSON encode and decode functions. The functions in `Cloogle.DB` provide
low-level access to modify the database and search in it.

The `Cloogle.DB.Factory` module hooks into the compiler to provide functions to
populate a database by parsing source code.

The functions in `Cloogle.Search` provide a higher level API to the database,
using the common Cloogle types defined by
[libcloogle](https://gitlab.com/cloogle/libcloogle).

## Authors & License
Cloogle is maintained by [Camil Staps][].

Copyright is owned by the authors of individual commits, including:

- Camil Staps
- Mart Lubbers

This project is licensed under AGPL v3 with additional terms under section 7;
see the [LICENSE](/LICENSE) file for details.

[Camil Staps]: https://camilstaps.nl
[Clean]: https://clean-lang.org

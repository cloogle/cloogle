# Changelog

#### v3.2.1

- Fix: prevent crashes when processing code using expression type syntax.

#### v3.2.0

- Chore: accept syntax new in lib-compiler 4.

#### v3.1.2

- Fix: fix compilation of Cloogle.DB with base-lib>=2.3. Cloogle 3.1.0–1 only
  work with base-lib<2.3.

#### v3.1.1

- Fix: fix sorting of results by distance.

### v3.1.0

- Chore: update to base 2.0.

## v3.0.0

- Feature: add package name to `UniqueResultIdentifier` in Cloogle.Search.Rank
  for disambiguation.
- Change: renamed `CloogleDB.names` to `fulltext`.
- Change: renamed `CloogleDB.exact_names` to `names`.

#### v2.0.1

- Enhancement: improve how the words in a query are combined; multiply instead
  of add scores to prefer results in which many words from the query are found.
- Enhancement: improve splitting words in camelCase names for full-text
  indexing.
- Fix: give constant weight to entry names in the full-text index (previously
  the weight of the name would depend on the length of the documentation, as
  they were included in the same index).
- Fix: include stopwords in the full-text entry when they occur in an entry
  name. This makes e.g. the function `o` searchable.

## v2.0.0

High-level changes:

- Feature: also index documentation and description and search on these when
  using `name` search.
- Feature: add support for indexing
  [common problems](https://gitlab.com/cloogle/common-problems).
- Enhancement: improve performance of syntax searches.
- Change: when a 'child' entry and a 'parent' entry are both returned from a
  search (e.g., a constructor and the corresponding type definition), if the
  child scores better than the parent, the child is not removed from the search
  results any more (this now only happens when the child is worse than the
  parent).

Changes to APIs:

- Feature: added constructor `CommonProblemEntry` to `CloogleEntry`.
- Enhancement: added field `n_common_problems` to `CloogleDBStats`.
- Enhancement: added type `SyntaxPattern` (either an exact pattern or a regex),
  and use this in `syntax_patterns` field of `SyntaxEntry`.
- Changed: replaced `CloogleDB.name_ngrams` with `names`, an index which also
  contains documentation and descriptions.
- Changed: renamed `CloogleDB.name_map` to `exact_names`.
- Changed: replaced `Annotation` constructors `MatchingNGramsQuery` and
  `MatchingNGramsResult` with `NameWeight`.
- Changed: replaced `RankSettings` fields `rs_matching_ngrams_q` and
  `rs_matching_ngrams_r` with `rs_name_weight`.
- Changed: replaced `removeContainedEntries` with `combineContainedEntries`
  with slightly different behaviour.
- Changed: `filterName` now returns an optional error message.
- Changed: `search`, ``search` ``, and `searchWithSuggestions` now return
  optional error messages.
- Removed: removed `rs_record_field` from `RankSettings` as it had not been
  used for a long time.

#### v1.1.4

- Chore: use `lib-compiler` instead of `lib-compiler-itasks`; update to v3.0.

#### v1.1.3

- Chore: move to https://clean-lang.org.

#### v1.1.2

- Chore: update compiler version to v2.0.

#### v1.1.1

- Chore: update compiler version to v1.1.0.

### v1.1

- Enhancement: add suggestion to include core modules, if the first result is
  better.
- Enhancement: add version information to the database.

#### v1.0.1

- Fix: modules from base-stdenv get an increased 'usage' count, because they are
  frequently imported with `import StdEnv` instead of directly. This was
  already implemented but broke when the library StdEnv was renamed to
  base-stdenv.

## v1.0.0

First tagged version

definition module Text.Stopwords

/**
 * This module contains a map with the NLTK stopwords
 * (https://www.nltk.org/nltk_data/).
 */

isStopword :: !String -> Bool

stopwords :: [String]

definition module Text.WordsFromCode

/**
 * Copyright 2016-2023 the authors (see README.md).
 *
 * This file is part of Cloogle.
 *
 * Cloogle is free software: you can redistribute it and/or modify it under the
 * terms of the GNU Affero General Public License as published by the Free
 * Software Foundation, version 3 of the License.
 *
 * Cloogle is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE.  See the GNU Affero General Public License for
 * more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with Cloogle. If not, see <https://www.gnu.org/licenses/>.
 *
 * The software is licensed under additional terms under section 7 of the GNU
 * Affero General Public License; see the LICENSE file for details.
 *
 * @property-bootstrap
 *   import StdEnv
 */

/**
 * Gets all words from a string, splitting on whitespace and punctuation. All
 * words are returned in lower case.
 *
 * This function is meant in contexts where the string may contain words that
 * are function names from source code. Essentially, it performs
 * {{`splitOnDigits`}} after {{`splitOnCamelCase`}} after
 * {{`splitOnSnakeCase`}} after {{`words`}}, finally transforming each word to
 * lower case.
 *
 * When words in `snake_case` or `camelCase` are detected, both the entire word
 * and the parts are included:
 * - `wordsFromCode "snake_case" = ["snakecase", "snake", "case"]`
 * - `wordsFromCode "camelCase" = ["camelcase", "camel", "case"]`
 * - Digits may be part of words, e.g. `column2`, `column3`
 *
 * @property examples:
 *   wordsFromCode "viewInformation is an iTasks function, which displays a value." =.=
 *     [ "viewinformation", "view", "information", "is", "an", "itasks", "i", "tasks", "function"
 *     , "which", "displays", "a", "value"
 *     ] /\
 *   wordsFromCode "To transform a `?` value to a list you can use `mb2list`." =.=
 *     ["to", "transform", "a", "value", "to", "a", "list", "you", "can", "use", "mb2list", "mb", "list"]
 */
wordsFromCode :: !String -> [String]

/**
 * Gets all words from a string, splitting on whitespace and punctuation.
 *
 * @property example:
 *   words "A horse, a horse -- my kingdom for a horse!" =.=
 *   ["A", "horse", "a", "horse", "my", "kingdom", "for", "a", "horse"]
 */
words :: !String -> [String]

/**
 * This is like {{`splitOnSnakeCase`}}, but splits on digits instead of
 * underscore characters. This is useful for splitting words like `list2mb` or
 * `word2vec`.
 *
 * @property example:
 *   splitOnDigits "word2vec" =.= ["word", "vec"]
 */
splitOnDigits :: !String -> [String]

/**
 * Returns all words in a `camelCase` symbol name. Consecutive uppercase
 * letters are taken to start a new word; digits are not.
 *
 * @property examples:
 *   splitOnCamelCase "startANewZ80Assembler" =.=
 *     ["start", "A", "New", "Z80", "Assembler"] /\
 *   splitOnCamelCase "CleanDB" =.=
 *     ["Clean", "DB"] /\
 *   splitOnCamelCase "BVInt" =.=
 *     ["BV", "Int"]
 */
splitOnCamelCase :: !String -> [String]

/**
 * Returns all words in a `snake_case` symbol name. This function ignores
 * double, leading, and trailing underscores.
 *
 * @property example:
 *   splitOnSnakeCase "__exam____pl_e_" =.= ["exam", "pl", "e"]
 */
splitOnSnakeCase :: !String -> [String]

implementation module Text.WordsFromCode

/**
 * Copyright 2016-2023 the authors (see README.md).
 *
 * This file is part of Cloogle.
 *
 * Cloogle is free software: you can redistribute it and/or modify it under the
 * terms of the GNU Affero General Public License as published by the Free
 * Software Foundation, version 3 of the License.
 *
 * Cloogle is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE.  See the GNU Affero General Public License for
 * more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with Cloogle. If not, see <https://www.gnu.org/licenses/>.
 *
 * The software is licensed under additional terms under section 7 of the GNU
 * Affero General Public License; see the LICENSE file for details.
 */

import StdEnv

wordsFromCode :: !String -> [String]
wordsFromCode s =
	[ {toLower c \\ c <-: w4}
	\\ w1 <- words s
	, w2 <- splitOnSnakeCase` w1
	, w3 <- splitOnCamelCase` w2
	, w4 <- splitOnDigits` w3
	]
where
	splitOnDigits` s = case splitOnDigits s of
		[s] -> [s]
		ws -> [s:ws]
	splitOnCamelCase` s = case splitOnCamelCase s of
		[s] -> [s]
		ws -> [s:ws]
	splitOnSnakeCase` s = case splitOnSnakeCase s of
		[s] -> [s]
		ws -> [s:ws]

words :: !String -> [String]
words s = split (\c -> not (isAlphanum c)) inc s

splitOnDigits :: !String -> [String]
splitOnDigits s = split isDigit inc s

splitOnCamelCase :: !String -> [String]
splitOnCamelCase s = run 0 1
where
	run last i
		| i >= size s
			= this_word i
		# c = s.[i]
		| not (isAlpha c)
			= run last (i+1)
		| isUpper c
			| let prev = s.[i-1] in isAlpha prev && isLower prev
				= this_word i ++ run i (i+1)
			| i < size s-1 && let next = s.[i+1] in isAlpha next && isLower next
				= this_word i ++ run i (i+1)
				= run last (i+1)
		| otherwise
			= run last (i+1)
	where
		this_word i
			| i > last
				| last == 0 && i == size s-1
					= [s]
					= [s % (last, i-1)]
				= []

splitOnSnakeCase :: !String -> [String]
splitOnSnakeCase s = split (\c -> c == '_') inc s

split startNewWord newWordStartIndex s :== run 0 0
where
	run last i
		| i >= size s
			= this_word
		| startNewWord s.[i]
			= this_word ++ run (newWordStartIndex i) (i+1)
			= run last (i+1)
	where
		this_word
			| i > last
				| last == 0 && i == size s-1
					= [s]
					= [s % (last, i-1)]
				= []

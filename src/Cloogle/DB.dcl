definition module Cloogle.DB

/**
 * Data types and functions for the Cloogle database.
 *
 * Copyright 2016-2023 the authors (see README.md).
 *
 * This file is part of Cloogle.
 *
 * Cloogle is free software: you can redistribute it and/or modify it under the
 * terms of the GNU Affero General Public License as published by the Free
 * Software Foundation, version 3 of the License.
 *
 * Cloogle is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE.  See the GNU Affero General Public License for
 * more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with Cloogle. If not, see <https://www.gnu.org/licenses/>.
 *
 * The software is licensed under additional terms under section 7 of the GNU
 * Affero General Public License; see the LICENSE file for details.
 */

from StdOverloaded import class ==, class <, class zero
from StdClass import class Ord

from Clean.Doc import :: FunctionDoc, :: TypeDoc, :: ClassDoc, :: ModuleDoc
from Clean.Types import :: Type, :: TypeVar, :: TVAssignment, :: TypeDef,
	:: TypeContext, :: TypeDefRhs, :: TypeRestriction, :: Priority, :: Unifier
from Clean.Types.Tree import :: TypeTree
from Clean.Types.Util import class print(..)

from Data.GenEq import generic gEq
from Data.Map import :: Map
from Database.Native import :: NativeDB, :: Entry, :: Index
from System.FilePath import :: FilePath
from Text.GenJSON import generic JSONEncode, generic JSONDecode, :: JSONNode

from SPDX.License import :: License

from Regex import :: CompiledRegex

from Cloogle.API import :: ABCArgument, :: CleanLangReportLocation,
	:: FunctionKind, :: LocationResult, :: ProblemResultExtras,
	:: SyntaxExample
from Cloogle.DB.FullText import :: FullTextIndex

NGRAMS_N  :== 3
NGRAMS_CI :== True

/**
 * A storage for function types, class definitions, type definitions, etc.
 */
:: *CloogleDB =
	{ db            :: !*NativeDB CloogleEntry Annotation //* Core data
	, fulltext      :: !FullTextIndex Index //* Index for names, documentation, etc.
	, names         :: !Map Name {#Index} //* For exact name search
	, types         :: !TypeTree Index    //* Types, map to FunctionEntries
	, core          :: !{#Index}          //* Entries in core modules
	, builtins      :: !{#Index}          //* Language builtins
	, syntax        :: !{#Index}          //* SyntaxEntries
	, abc_instrs    :: !{#Index}          //* ABCInstructionEntries
	, type_synonyms :: !{#Index}          //* TypeDefEntries of TDRSynonym
	, library_map   :: !Map Name {#Index} //* Entries by library name
	, module_map    :: !Map Name {#Index} //* Entries by module name
	, derive_map    :: !Map Name {#Index} //* DeriveEntries by generic name
	, instance_map  :: !Map Name {#Index} //* InstanceEntries by class name
	, always_unique :: !Map Name ()       //* Types that are always unique, like World
	, licenses      :: !Map Library License //* The license for each library
	}

/**
 * Information about a {{`CloogleDB`}}. Can be retrieved with {{`dbStats`}}.
 */
:: CloogleDBStats =
	{ n_modules             :: Int
	, n_functions           :: Int
	, n_functions_with_type :: Int
	, n_unique_types        :: Int
	, type_tree_depth       :: Int
	, n_type_definitions    :: Int
	, n_classes             :: Int
	, n_instances           :: Int
	, n_derivations         :: Int
	, n_syntax_constructs   :: Int
	, n_abc_instructions    :: Int
	, n_common_problems     :: Int
	}

//* The type context required for a unification search.
:: RequiredContext =
	{ representation :: String
	, only_free_vars :: !Bool //* There are only free variables in this context
	, locations      :: ![LocationResult]
	}

/**
 * Annotations to store during search.
 */
:: Annotation
	= NameWeight !Real  //* The Tf-Idf score,for name search
	| Unifier !Unifier  //* For type search, the unifier
	| RequiredContext ![RequiredContext] //* For type search, context after unification
	| UsedSynonyms !Int //* The number of synonyms used for unification
	| ExactResult       //* If this was an exact match found with filterExactName

/**
 * Wrapper around different kinds of entries to store all in one database.
 */
:: CloogleEntry
	= FunctionEntry       !FunctionEntry
	| TypeDefEntry        !TypeDefEntry
	| ModuleEntry         !ModuleEntry
	| ClassEntry          !ClassEntry
	| InstanceEntry       !InstanceEntry
	| DeriveEntry         !DeriveEntry
	| SyntaxEntry         !SyntaxEntry
	| ABCInstructionEntry !ABCInstructionEntry
	| CommonProblemEntry  !ProblemResultExtras

derive JSONEncode CloogleEntry
derive JSONDecode CloogleEntry

/**
 * The indexes of entries using an entry. This is empty for `InstanceEntry`,
 * `DeriveEntry`, `SyntaxEntry`, and `ABCInstructionEntry`, because for these
 * entries we do not track usages.
 */
usages :: !CloogleEntry -> {#Index}

/**
 * The number of usages (imports) of the module corresponding that an entry is
 * defined in. This is 0 for `SyntaxEntry` and `ABCInstructionEntry`, because
 * these are not defined in any module. Also `InstanceEntry` and `DeriveEntry`
 * have 0, because module usages are only used for ranking and these types are
 * never directly returned as a result.
 */
moduleUsages :: !CloogleEntry -> Int

/**
 * A location in a Clean library, or a builtin.
 */
:: Location
	= Location !Library !String !Module !FilePath !LineNr !LineNr !Name //* A normal location
	| Builtin !Name ![CleanLangReportLocation] //* A language builtin
	| NoLocation //* Only used internally

/**
 * Wrapper around {{`Location`}} for use in {{`CloogleDBFactory`}} to avoid
 * name clashes with {{`Module`}} in the compiler.
 */
location :: !Library !String !String !FilePath !LineNr !LineNr !Name -> Location

/**
 * Not-type information that is often associated with things that have a type
 */
:: FunctionEntry =
	{ fe_loc            :: !Location      //* The location
	, fe_kind           :: !FunctionKind  //* The type of entry
	, fe_type           :: ! ?Type        //* The type, `?None` for macros
	, fe_priority       :: ! ?Priority    //* The infix priority
	, fe_generic_vars   :: ! ?[String]    //* The names of the type variables of a generic function
	  // Using TypeVar causes import clashes in CloogleDBFactory
	, fe_representation :: ! ?String      //* A string representation of the entry
	, fe_documentation  :: ! ?FunctionDoc //* Documentation on this entry
	, fe_typedef        :: ! ?Index       //* The TypeDefEntry for constructors/record fields
	, fe_class          :: ! ?Index       //* The class, for class members
	, fe_derivations    :: ! ?{#Index}    //* The DerivaionEntries
	, fe_usages         :: !{#Index}      //* FunctionEntries where the implementation uses this function
	, fe_module_usages  :: !Int           //* The number of times the corresponding module is imported
	}

/**
 * A TypeDef with meta-data
 */
:: TypeDefEntry =
	{ tde_loc           :: !Location  //* The location
	, tde_typedef       :: !TypeDef   //* The TypeDef
	, tde_doc           :: ! ?TypeDoc //* Documentation on the TypeDef
	, tde_instances     :: !{#Index}  //* Instances of this type
	, tde_derivations   :: !{#Index}  //* Derivations of this type
	, tde_usages        :: !{#Index}  //* FunctionEntries using the type
	, tde_module_usages :: !Int       //* The number of times the corresponding module is imported
	}

/**
 * Information about a module
 */
:: ModuleEntry =
	{ me_loc           :: !Location    //* The location
	, me_is_core       :: !Bool        //* Whether this is a core module (e.g. the os* modules in ObjectIO and TCPIP)
	, me_documentation :: ! ?ModuleDoc //* Documentation on this module
	, me_usages        :: !{#Index}    //* Modules importing this module
	}

/**
 * Information about a class
 */
:: ClassEntry =
	{ ce_loc           :: !Location    //* The location
	, ce_vars          :: ![Type]      //* The type variables of the class
	  // Using TypeVar causes import clashes in CloogleDBFactory
	, ce_is_meta       :: !Bool        //* Whether this is a meta class (no non-macro members and not TC)
	, ce_context       :: !TypeContext //* A class context
	, ce_documentation :: ! ?ClassDoc  //* Documentation on this class
	, ce_members       :: !{#Index}    //* Class members (FunctionEntries; must be lazy for CloogleDBFactory to work)
	, ce_instances     :: !{#Index}    //* All instances of the class
	, ce_derivations   :: !{#Index}    //* Derivations of generic meta-classes like iTask
	, ce_usages        :: !{#Index}    //* FunctionEntries, TypeDefEntries and ClassEntries using this class
	, ce_module_usages :: !Int         //* The number of times the corresponding module is imported
	}

/**
 * Information about a class instance
 */
:: InstanceEntry
	= { ie_class     :: !Name             //* The class
	  , ie_types     :: ![(Type, String)] //* The instantiated type and a string representation for each class variable
	  , ie_locations :: ![Location]       //* The places where this instance is found
	  }

/**
 * Information about a generic derivation
 */
:: DeriveEntry
	= { de_generic             :: !Name       //* The generic
	  , de_type                :: !Type       //* The type to derive an instance for
	  , de_type_representation :: !String     //* A string representation of the type
	  , de_locations           :: ![Location] //* The locations in which the derivation occurs
	  }

/**
 * Information about a syntax construct
 * This is very similar to {{`SyntaxResultExtras`}}, but also includes a
 * description string.
 */
:: SyntaxEntry =
	{ syntax_title         :: !String //* The name of the construct
	, syntax_patterns      :: ![SyntaxPattern] //* Patterns to search for the construct
	, syntax_code          :: ![String] //* Strings describing the construct, as short as possible
	, syntax_description   :: !String //* A description for documentation
	, syntax_doc_locations :: ![CleanLangReportLocation] //* Where to find documentation on the construct
	, syntax_examples      :: ![SyntaxExample] //* Some code examples (should include comments)
	}

:: SyntaxPattern
	= ExactPattern !String
	| RegexPattern !CompiledRegex

/**
 * Information about an ABC instruction.
 * This is very similar to {{`ABCInstructionResultExtras`}}, but also includes
 * a description string.
 */
:: ABCInstructionEntry =
	{ aie_instruction :: !String        //* The name of the instruction
	, aie_arguments   :: ![ABCArgument] //* The arguments
	, aie_description :: !String        //* A description for documentation
	}

:: Name    :== String
:: Library :== String
:: Module  :== String
:: LineNr  :== ?Int

instance zero Location
instance == Location
instance < Location
instance zero FunctionEntry
instance zero ModuleEntry

instance print (!Name, !FunctionEntry)

class getLocation a :: !a -> ?Location
instance getLocation FunctionEntry
instance getLocation TypeDefEntry
instance getLocation ModuleEntry
instance getLocation ClassEntry
instance getLocation CloogleEntry

getLibrary :: !Location -> ?Name
getVersion :: !Location -> ?String
getModule :: !Location -> ?Name
setModule :: !Name !Location -> Location
getFilename :: !Location -> ?String
getDclLine :: !Location -> ?Int
getIclLine :: !Location -> ?Int
getName :: !Location -> Name
setName :: !Name !Location -> Location
isBuiltin :: !Location -> Bool

toTypeDefEntry :: !Location !TypeDef !(?TypeDoc) -> TypeDefEntry
getTypeDef :: !TypeDefEntry -> TypeDef
getTypeDefDoc :: !TypeDefEntry -> ?TypeDoc
mergeTypeDefEntries :: !TypeDefEntry !TypeDefEntry -> TypeDefEntry

/**
 * Wrapper around the Class record field to work around name clashes
 *
 * @param The location of the class
 * @param The type variables of the class
 * @param Whether this is a meta class
 * @param The class context
 * @param The documentation
 * @result A Class record with those data
 */
toClass :: !Location ![Type] !Bool !TypeContext !(?ClassDoc) -> ClassEntry
classContext :: !ClassEntry -> [TypeRestriction]

saveDB :: !*CloogleDB !*File -> *(!*CloogleDB, !*File)
openDB :: !*File -> *(! ? *CloogleDB, !*File)

/**
 * Reset the database (count everything as included again).
 */
resetDB :: !*CloogleDB -> *CloogleDB

dbStats :: !*CloogleDB -> *(!CloogleDBStats, !*CloogleDB)

/**
 * Write the type tree as dot graph to a file.
 */
writeTypeTree :: !*CloogleDB !*File -> *(!*CloogleDB, !*File)

getValueByIndex :: !Index !*CloogleDB -> *(!CloogleEntry, !*CloogleDB)
getValuesByIndices` :: !{#Index} !*CloogleDB -> *(![!CloogleEntry!], !*CloogleDB)

filterDB :: (CloogleEntry -> Bool) !*CloogleDB -> *CloogleDB
excludeCore :: !*CloogleDB -> *CloogleDB
excludeBuiltins :: !*CloogleDB -> *CloogleDB
includeBuiltins :: !*CloogleDB -> *CloogleDB
filterLibraries :: ![Name] !*CloogleDB -> *CloogleDB
filterModules :: ![Name] !*CloogleDB -> *CloogleDB
filterName :: !String !*CloogleDB -> (!*CloogleDB, !?String)
filterExactName :: !String !*CloogleDB -> *CloogleDB
filterUnifying :: !Type !*CloogleDB -> *CloogleDB

filterUsages :: !(*CloogleDB -> *CloogleDB) ![String] !*CloogleDB -> *CloogleDB

allTypeSynonyms :: !*CloogleDB -> *(Map Name [TypeDef], !*CloogleDB)
alwaysUniquePredicate :: !*CloogleDB -> *(!(String -> Bool), !*CloogleDB)
getExactNameMatches :: !Name !*CloogleDB -> *(![!CloogleEntry!], !*CloogleDB)
getInstances :: !Name !*CloogleDB -> *(![InstanceEntry], !*CloogleDB)
getDerivations :: !Name !*CloogleDB -> *(![DeriveEntry], !*CloogleDB)

/**
 * E.g., if there is some constructor in the result list, but also its
 * corresponding type definition with a lower distance, remove the constructor
 * and lower the type definition's distance even more.
 */
combineContainedEntries :: !*CloogleDB -> *CloogleDB

getEntries :: !*CloogleDB -> *(![!(CloogleEntry, [!Annotation!])!], !*CloogleDB)

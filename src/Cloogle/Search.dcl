definition module Cloogle.Search

/**
 * Search functions for the Cloogle system.
 *
 * Copyright 2016-2023 the authors (see README.md).
 *
 * This file is part of Cloogle.
 *
 * Cloogle is free software: you can redistribute it and/or modify it under the
 * terms of the GNU Affero General Public License as published by the Free
 * Software Foundation, version 3 of the License.
 *
 * Cloogle is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE.  See the GNU Affero General Public License for
 * more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with Cloogle. If not, see <https://www.gnu.org/licenses/>.
 *
 * The software is licensed under additional terms under section 7 of the GNU
 * Affero General Public License; see the LICENSE file for details.
 */

from Clean.Types import :: TypeDef, :: Type, :: Unifier
from Data.Error import :: MaybeError
from Data.Map import :: Map
from Database.Native import :: NativeDB

from Cloogle.API import :: Request, :: Result, :: LocationResult
from Cloogle.DB import :: CloogleDB, :: RequiredContext, :: Annotation,
	:: CloogleEntry, :: FunctionEntry
from Cloogle.Search.Rank import :: RankSettings

/**
 * Cloogle setting: whether to include language builtins if the Request has
 * `?None` for the include_builtin field.
 */
DEFAULT_INCLUDE_BUILTINS :== True

/**
 * Cloogle setting: whether to include library core modules if the Request has
 * `?None` for the include_core field.
 */
DEFAULT_INCLUDE_CORE :== False

/**
 * Search for a request in the type database
 */
search :: !Request !*CloogleDB -> *(!MaybeError String [Result], !*CloogleDB)

search` :: !Request !*CloogleDB ->
	*(!?String
	, !?Type
	, !(Map String [TypeDef])
	, ![TypeDef]
	, ![!(CloogleEntry, [!Annotation!])!]
	, !*CloogleDB
	)

unifyInformation :: !(?Type) !(Map String [TypeDef]) ![TypeDef] !FunctionEntry !*CloogleDB
	-> *(!?Unifier, ![TypeDef], ![RequiredContext], !*CloogleDB)

/**
 * Search for a request, and also make suggestions for similar requests with
 * better results.
 */
searchWithSuggestions :: !Request !*CloogleDB -> *(!MaybeError String [Result], ![(Request,[Result])], !*CloogleDB)

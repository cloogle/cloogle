implementation module Cloogle.DB

/**
 * Copyright 2016-2023 the authors (see README.md).
 *
 * This file is part of Cloogle.
 *
 * Cloogle is free software: you can redistribute it and/or modify it under the
 * terms of the GNU Affero General Public License as published by the Free
 * Software Foundation, version 3 of the License.
 *
 * Cloogle is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE.  See the GNU Affero General Public License for
 * more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with Cloogle. If not, see <https://www.gnu.org/licenses/>.
 *
 * The software is licensed under additional terms under section 7 of the GNU
 * Affero General Public License; see the LICENSE file for details.
 */

import _SystemStrictLists
import StdArray
import StdBool
import StdFile
from StdFunc import id, o, const
import StdList
import StdMisc
import StdOrdList
import StdOverloaded
import StdOverloadedList
import StdString
import StdTuple

import Clean.Doc
import Clean.Types
import Clean.Types.Tree
import Clean.Types.Util
import Control.Applicative
import Control.Monad
import Data.Bifunctor
import Data.Error
from Data.Foldable import class Foldable(foldr1()), instance Foldable []
from Data.Func import $, on, `on`, mapSt
import Data.Functor
import Data.GenLexOrd
import Data.Graphviz
from Data.List import concatMap, groupBy, intercalate, intersect, tails
from Data.Map import :: Map(..), elems, filterWithKey, foldrNoKey,
	foldrWithKey, fromList, get, mapSize, alter, mapWithKey, newMap, put,
	toAscList, toList, instance Functor (Map k)
import Data.Maybe
import Data.Tuple
from Database.Native import :: NativeDB, :: Index, :: Entry{..},
	:: SearchMode(..), instance == Index, instance < Index
import qualified Database.Native
import Database.Native.JSON
import System.File
import System.FilePath
from Text import class Text(concat), instance Text String
import Text.GenJSON

import SPDX.License

import qualified Regex
from Regex import :: CompiledRegex,
	derive JSONEncode CompiledRegex, derive JSONDecode CompiledRegex

import Cloogle.API
from Cloogle.DB.FullText import :: FullTextIndex,
	derive JSONEncode FullTextIndex, derive JSONDecode FullTextIndex
import qualified Cloogle.DB.FullText

JSONEncode{|{#Index}|} infield xs = JSONEncode{|*|} infield [x \\ x <-: xs]
JSONDecode{|{#Index}|} infield json = case JSONDecode{|*|} infield json of
	(?Just xs,json) -> (?Just {#x \\ x <- xs},json)
	(?None,   json) -> (?None,json)

derive JSONEncode Map
derive JSONDecode Map

derive JSONEncode ABCInstructionEntry, ClassDoc, ClassEntry, ClassMemberDoc,
	CloogleEntry, Constructor, ConstructorDoc, DeriveEntry, FunctionDoc,
	FunctionEntry, InstanceEntry, Location, ModuleDoc, ModuleEntry,
	MultiLineString, ParamDoc, Priority, ProblemResultExtras, Property,
	PropertyBootstrapDoc, PropertyTestGenerator, PropertyVarInstantiation,
	RecordField, SyntaxEntry, SyntaxPattern, Type, TypeContext, TypeDef,
	TypeDefEntry, TypeDefRhs, TypeDoc, TypeRestriction
derive JSONDecode ABCInstructionEntry, ClassDoc, ClassEntry, ClassMemberDoc,
	CloogleEntry, Constructor, ConstructorDoc, DeriveEntry, FunctionDoc,
	FunctionEntry, InstanceEntry, Location, ModuleDoc, ModuleEntry,
	MultiLineString, ParamDoc, Priority, ProblemResultExtras, Property,
	PropertyBootstrapDoc, PropertyTestGenerator, PropertyVarInstantiation,
	RecordField, SyntaxEntry, SyntaxPattern, Type, TypeContext, TypeDef,
	TypeDefEntry, TypeDefRhs, TypeDoc, TypeRestriction

printersperse :: Bool a [b] -> [String] | print a & print b
printersperse ia a bs = intercalate (print False a) (map (print ia) bs)

(--) infixr 5 :: a b -> [String] | print a & print b
(--) a b = print False a ++ print False b

instance zero Location where zero = NoLocation
derive gLexOrd Location, CleanLangReportLocation

instance < Location
where
	(<) (Location l1 v1 m1 _ d1 i1 n1) (Location l2 v2 m2 _ d2 i2 n2)
		= ((l1,v1,m1),(n1,d1,i1)) < ((l2,v2,m2),(n2,d2,i2))
	(<) (Location _ _ _ _ _ _ _) _
		= True
	(<) _ (Location _ _ _ _ _ _ _)
		= False
	(<) (Builtin a _) (Builtin b _)
		= a < b
	(<) (Builtin _ _) _
		= True
	(<) _ _
		= False

instance == Location
where
	(==) (Location a b c d e f g) (Location p q r s t u v)
		= a==p && b==q && c==r && d==s && e==t && f==u && g==v
	(==) (Builtin a _) (Builtin b _) = a == b
	(==) NoLocation NoLocation = True
	(==) _ _ = False

instance zero FunctionEntry
where
	zero =
		{ fe_loc            = zero
		, fe_kind           = Function
		, fe_type           = ?None
		, fe_priority       = ?None
		, fe_generic_vars   = ?None
		, fe_representation = ?None
		, fe_documentation  = ?None
		, fe_typedef        = ?None
		, fe_class          = ?None
		, fe_derivations    = ?None
		, fe_usages         = {}
		, fe_module_usages  = 0
		}

instance zero ModuleEntry
where
	zero =
		{ me_loc           = zero
		, me_is_core       = False
		, me_documentation = ?None
		, me_usages        = {}
		}

instance print (!Name, !FunctionEntry)
where
	print b (f, fe)
		= gen -- fname -- " " -- prio -- vars -- if (isJust fe.fe_type) (":: " -- fe.fe_type) []
	where
		prio = case fe.fe_priority of
			?None -> []
			?Just p -> print b p -- " "
		vars = case fe.fe_generic_vars of
			?None -> []
			?Just vs -> printersperse b " " vs -- " "
		gen = if (isJust fe.fe_generic_vars) "generic " ""
		fname
		| isJust fe.fe_priority     = concat ("(" -- f -- ")")
		| fe.fe_kind == RecordField = "." +++ f
		| otherwise                 = f

usages :: !CloogleEntry -> {#Index}
usages (FunctionEntry fe) = fe.fe_usages
usages (TypeDefEntry tde) = tde.tde_usages
usages (ModuleEntry me) = me.me_usages
usages (ClassEntry ce) = ce.ce_usages
usages (InstanceEntry ie) = {}
usages (DeriveEntry de) = {}
usages (SyntaxEntry _) = {}
usages (ABCInstructionEntry _) = {}
usages (CommonProblemEntry _) = {}

moduleUsages :: !CloogleEntry -> Int
moduleUsages (FunctionEntry fe) = fe.fe_module_usages
moduleUsages (TypeDefEntry tde) = tde.tde_module_usages
moduleUsages (ModuleEntry me) = size me.me_usages
moduleUsages (ClassEntry ce) = ce.ce_module_usages
moduleUsages (InstanceEntry ie) = 0
moduleUsages (DeriveEntry de) = 0
moduleUsages (SyntaxEntry _) = 0
moduleUsages (ABCInstructionEntry _) = 0
moduleUsages (CommonProblemEntry _) = 0

location :: !Library !String !String !FilePath !LineNr !LineNr !Name -> Location
location lib v mod fp dcl icl name = Location lib v mod fp dcl icl name

instance getLocation FunctionEntry where getLocation fe  = ?Just fe.fe_loc
instance getLocation TypeDefEntry  where getLocation tde = ?Just tde.tde_loc
instance getLocation ModuleEntry   where getLocation me  = ?Just me.me_loc
instance getLocation ClassEntry    where getLocation ce  = ?Just ce.ce_loc
instance getLocation SyntaxEntry   where getLocation se  = ?Just $ Builtin se.SyntaxEntry.syntax_title se.syntax_doc_locations
instance getLocation CloogleEntry
where
	getLocation (FunctionEntry e) = getLocation e
	getLocation (TypeDefEntry e)  = getLocation e
	getLocation (ModuleEntry e) = getLocation e
	getLocation (ClassEntry e) = getLocation e
	getLocation (SyntaxEntry e) = getLocation e
	getLocation _ = ?None

getLibrary :: !Location -> ?Name
getLibrary (Location lib _ _ _ _ _ _) = ?Just lib
getLibrary _ = ?None

getVersion :: !Location -> ?String
getVersion (Location _ v _ _ _ _ _) = ?Just v
getVersion _ = ?None

getModule :: !Location -> ?Name
getModule (Location _ _ mod  _ _ _ _) = ?Just mod
getModule _ = ?None

setModule :: !Name !Location -> Location
setModule m (Location lib v _ fname dcl icl n) = Location lib v m fname dcl icl n
setModule _ l                                = l

getFilename :: !Location -> ?String
getFilename (Location _ _ _ fn  _ _ _) = ?Just fn
getFilename _ = ?None

getDclLine :: !Location -> ?Int
getDclLine (Location _ _ _ _ dcl  _ _) = dcl
getDclLine _ = ?None

getIclLine :: !Location -> ?Int
getIclLine (Location _ _ _ _ _ icl  _) = icl
getIclLine _ = ?None

getName :: !Location -> Name
getName (Location _ _ _ _ _ _ name) = name
getName (Builtin name _)            = name
getName NoLocation                  = abort "getName NoLocation called\n"

setName :: !Name !Location -> Location
setName n (Location lib v mod fname dcl icl _) = Location lib v mod fname dcl icl n
setName n (Builtin _ doc)                      = Builtin n doc
setName _ NoLocation                           = abort "setName NoLocation called\n"

isBuiltin :: !Location -> Bool
isBuiltin (Builtin _ _) = True
isBuiltin _             = False

toTypeDefEntry :: !Location !TypeDef !(?TypeDoc) -> TypeDefEntry
toTypeDefEntry loc td doc =
	{ tde_loc=loc
	, tde_typedef=td
	, tde_doc=doc
	, tde_instances={}
	, tde_derivations={}
	, tde_usages={}
	, tde_module_usages=0
	}

getTypeDef :: !TypeDefEntry -> TypeDef
getTypeDef {tde_typedef} = tde_typedef

getTypeDefDoc :: !TypeDefEntry -> ?TypeDoc
getTypeDefDoc {tde_doc} = tde_doc

mergeTypeDefEntries :: !TypeDefEntry !TypeDefEntry -> TypeDefEntry
mergeTypeDefEntries a=:{tde_typedef={td_rhs=TDRAbstract ?None}} b = case b.tde_typedef.td_rhs of
	TDRAbstract _ -> a
	rhs           -> {a & tde_typedef.td_rhs=TDRAbstract (?Just rhs)}
mergeTypeDefEntries a b = a

toClass :: !Location ![Type] !Bool !TypeContext !(?ClassDoc) -> ClassEntry
toClass loc vs meta cc doc =
	{ ce_loc           = loc
	, ce_vars          = vs
	, ce_is_meta       = meta
	, ce_context       = cc
	, ce_documentation = doc
	, ce_members       = {}
	, ce_instances     = {}
	, ce_derivations   = {}
	, ce_usages        = {}
	, ce_module_usages = 0
	}

classContext :: !ClassEntry -> [TypeRestriction]
classContext {ce_context=TypeContext tc} = tc

saveDB :: !*CloogleDB !*File -> *(!*CloogleDB, !*File)
saveDB wrapper=:{db,fulltext,names,types,core,builtins,syntax,abc_instrs,
	type_synonyms,library_map,module_map,derive_map,instance_map,always_unique,
	licenses} f
# (db,f) = 'Database.Native'.saveDB db f
# f = write fulltext f
# f = write names f
# f = write types f
# f = write core f
# f = write builtins f
# f = write syntax f
# f = write abc_instrs f
# f = write type_synonyms f
# f = write library_map f
# f = write module_map f
# f = write derive_map f
# f = write instance_map f
# f = write always_unique f
# f = write licenses f
= ({wrapper & db=db}, f)
where
	write :: a *File -> *File | JSONEncode{|*|} a
	write x f = f <<< toString (toJSON x) <<< '\n'

openDB :: !*File -> *(! ? *CloogleDB, !*File)
openDB f
# ((ok,db),f) = appFst isJustU $ 'Database.Native'.openDB f
| not ok = (?None, f)
# (fulltext,f) = read f
# (names,f) = read f
# (types,f) = read f
# (core,f) = read f
# (builtins,f) = read f
# (syntax,f) = read f
# (abc_instrs,f) = read f
# (type_synonyms,f) = read f
# (library_map,f) = read f
# (module_map,f) = read f
# (derive_map,f) = read f
# (instance_map,f) = read f
# (always_unique,f) = read f
# (licenses,f) = read f
= (
	fulltext >>= \fulltext ->
	names >>= \names ->
	types >>= \types ->
	core >>= \core ->
	builtins >>= \builtins ->
	syntax >>= \syntax ->
	abc_instrs >>= \abc_instrs ->
	type_synonyms >>= \type_synonyms ->
	library_map >>= \library_map ->
	module_map >>= \module_map ->
	derive_map >>= \derive_map ->
	instance_map >>= \instance_map ->
	always_unique >>= \always_unique ->
	licenses >>= \licenses -> ?Just
	{ db=fromJust db
	, fulltext=fulltext
	, names=names
	, types=types
	, core=core
	, builtins=builtins
	, syntax=syntax
	, abc_instrs=abc_instrs
	, type_synonyms=type_synonyms
	, library_map=library_map
	, module_map=module_map
	, derive_map=derive_map
	, instance_map=instance_map
	, always_unique=always_unique
	, licenses=licenses
	}, f)
where
	read :: *File -> *(?a, *File) | JSONDecode{|*|} a
	read f
	# (end,f) = fend f
	| end = (?None, f)
	# (line,f) = freadline f
	= (fromJSON (fromString line), f)

	(>>=) ?None _ = ?None // Overridden to deal with uniqueness
	(>>=) (?Just x) f = f x

resetDB :: !*CloogleDB -> *CloogleDB
resetDB wrap=:{db} = {wrap & db='Database.Native'.resetDB db}

dbStats :: !*CloogleDB -> *(!CloogleDBStats, !*CloogleDB)
dbStats wrap=:{db,types}
# (es,db) = 'Database.Native'.allEntries db
# stats = Foldr count zero es
= (stats, {wrap & db=db})
where
	count :: CloogleEntry CloogleDBStats -> CloogleDBStats
	count (ModuleEntry _)         st = {st & n_modules=st.n_modules+1}
	count (FunctionEntry _)       st = {st & n_functions=st.n_functions+1}
	count (ClassEntry _)          st = {st & n_classes=st.n_classes+1}
	count (TypeDefEntry _)        st = {st & n_type_definitions=st.n_type_definitions+1}
	count (InstanceEntry _)       st = {st & n_instances=st.n_instances+1}
	count (DeriveEntry _)         st = {st & n_derivations=st.n_derivations+1}
	count (SyntaxEntry _)         st = {st & n_syntax_constructs=st.n_syntax_constructs+1}
	count (ABCInstructionEntry _) st = {st & n_abc_instructions=st.n_abc_instructions+1}
	count (CommonProblemEntry _)  st = {st & n_common_problems=st.n_common_problems+1}

	zero :: CloogleDBStats
	zero =
		{ n_modules             = 0
		, n_functions           = 0
		, n_functions_with_type = typeTreeSize types
		, n_unique_types        = typeTreeNodes types
		, type_tree_depth       = typeTreeDepth types
		, n_type_definitions    = 0
		, n_classes             = 0
		, n_instances           = 0
		, n_derivations         = 0
		, n_syntax_constructs   = 0
		, n_abc_instructions    = 0
		, n_common_problems     = 0
		}

writeTypeTree :: !*CloogleDB !*File -> *(!*CloogleDB, !*File)
writeTypeTree db=:{types} f
# f = f <<< concat (printDigraph (typeTreeToGraphviz types))
= (db, f)

getValueByIndex :: !Index !*CloogleDB -> *(!CloogleEntry, !*CloogleDB)
getValueByIndex idx wrap=:{db}
# (val,db) = 'Database.Native'.getValueByIndex idx db
= (val, {wrap & db=db})

getValuesByIndices` :: !{#Index} !*CloogleDB -> *(![!CloogleEntry!], !*CloogleDB)
getValuesByIndices` idxs wrap=:{db}
# (es,db) = 'Database.Native'.getValuesByIndices` idxs db
= (es, {wrap & db=db})

filterDB :: (CloogleEntry -> Bool) !*CloogleDB -> *CloogleDB
filterDB f db = {db & db = 'Database.Native'.search Intersect (\v -> (f v, [!!])) db.db}

excludeCore :: !*CloogleDB -> *CloogleDB
excludeCore wrap=:{db,core}
# db = 'Database.Native'.unsearchIndices` core db
= {wrap & db=db}

excludeBuiltins :: !*CloogleDB -> *CloogleDB
excludeBuiltins wrap=:{db,builtins}
# db = 'Database.Native'.unsearchIndices` builtins db
= {wrap & db=db}

includeBuiltins :: !*CloogleDB -> *CloogleDB
includeBuiltins wrap=:{db,builtins}
# db = 'Database.Native'.searchIndices AddExcluded [(b,[!!]) \\ b <-: builtins] db
= {wrap & db=db}

filterLibraries :: ![Name] !*CloogleDB -> *CloogleDB
filterLibraries ss wrap=:{db,library_map}
# db = 'Database.Native'.searchIndices Intersect [(i,[!!]) \\ i <- idxs] db
= {wrap & db=db}
where
	idxs = foldr merge [] $ map (\xs->[x\\x<-:xs]) $ catMaybes $ map (flip get library_map) ss

filterModules :: ![Name] !*CloogleDB -> *CloogleDB
filterModules ss wrap=:{db,module_map}
# db = 'Database.Native'.searchIndices Intersect (map (flip tuple [!!]) idxs) db
= {wrap & db=db}
where
	idxs = foldr merge [] $ map (\xs->[x\\x<-:xs]) $ catMaybes $ map (flip get module_map) ss

filterName :: !String !*CloogleDB -> (!*CloogleDB, !?String)
filterName query wrap=:{db,fulltext,syntax,abc_instrs}
# mbIndices = 'Cloogle.DB.FullText'.search query fulltext
  indices = fromOk mbIndices
| isError mbIndices = (wrap, ?Just (fromError mbIndices))
# db = 'Database.Native'.searchIndices Intersect [(i, [!NameWeight w!]) \\ (i,w) <- indices] db
# db = addSyntax 0 syntax query db
# db = 'Database.Native'.searchWithIndices` (abcSearch query) abc_instrs db
= ({wrap & db=db}, ?None)

filterExactName :: !String !*CloogleDB -> *CloogleDB
filterExactName name wrap=:{db,names,syntax,abc_instrs}
# db = 'Database.Native'.searchIndices Intersect [(i,[!ExactResult!]) \\ i <-: idxs] db
# db = addSyntax 0 syntax name db
# db = 'Database.Native'.searchWithIndices` (abcSearch name) abc_instrs db
= {wrap & db=db}
where
	idxs = fromMaybe {} $ get name names

addSyntax :: !Int !{#Index} !String !*(NativeDB CloogleEntry Annotation) -> *NativeDB CloogleEntry Annotation
addSyntax i idxs query db
| i >= size idxs = db
# idx = idxs.[i]
# (inc,db) = 'Database.Native'.isIndexIncluded idx db
| inc
	// if the entry was found by name search, we don't need to check patterns (regexes are somewhat slow),
	// but we do boost the name weight because syntax results are important
	# (annots,db) = 'Database.Native'.getAnnotationsByIndex idx db
	# annots = Map (\a -> case a of NameWeight r -> NameWeight (r*10.0); a -> a) annots
	# db = 'Database.Native'.searchIndex idx annots db
	= addSyntax (i+1) idxs query db
# (entry,db) = 'Database.Native'.getValueByIndex idx db
# db = case entry of
	SyntaxEntry {syntax_patterns} | any matches syntax_patterns ->
		'Database.Native'.searchIndex idx [!ExactResult!] db
	_ ->
		db
= addSyntax (i+1) idxs query db
where
	matches (ExactPattern p) = p == query
	matches (RegexPattern p) = not (isEmpty ('Regex'.match p query))

abcSearch :: !String !CloogleEntry -> (!Bool, ![!Annotation!])
abcSearch query (ABCInstructionEntry ie) = (ie.aie_instruction == query, [!ExactResult!])
abcSearch _ _ = (False, [!!])

filterUnifying :: !Type !*CloogleDB -> *CloogleDB
filterUnifying t wrap=:{db,types}
# db = 'Database.Native'.searchIndices Intersect idxs db
= {wrap & db=db}
where
	idxs = sortBy ((<) `on` fst) [(idx,[!Unifier u!]) \\ (t,u,idxs) <- findUnifying t types, idx <- idxs]

filterUsages :: !(*CloogleDB -> *CloogleDB) ![String] !*CloogleDB -> *CloogleDB
filterUsages filter query wrap=:{names}
// For each name, the corresponding entries
# idxss = map (fromMaybe {#} o flip get names) query
# nameidxs = [(i,[!ExactResult!]) \\ i <- sort [i \\ is <- idxss, i <-: is]]
# wrap=:{db} = filter wrap
# db = 'Database.Native'.searchIndices Intersect nameidxs db
// For all lists of entries, the corresponding usages
# (valuess,db) = mapSt 'Database.Native'.getValuesByIndices` idxss db
# wrap & db = db
# wrap=:{db} = filter $ resetDB wrap
# usagess = [foldr mergeUnion [] [[u \\ u <-: getUsages value] \\ value <|- values] \\ values <- valuess]
// AND all usages together
# usages = case usagess of
	[] -> []
	us -> foldr1 mergeIntersect us
# (vals,db) = 'Database.Native'.getValuesByIndices usages db
# (sorted,unsorted) = collectUsages usages vals
# usages = mergeUnion sorted (removeDupSorted $ sort unsorted)
# db = 'Database.Native'.searchIndices Intersect (mergeUnionWithAnnots nameidxs [(u,[!!]) \\ u <- usages]) db
= {wrap & db=db}
where
	getUsages :: !CloogleEntry -> {#Index}
	getUsages (TypeDefEntry tde) = tde.tde_usages
	getUsages (ClassEntry ce)    = ce.ce_usages
	getUsages (ModuleEntry me)   = me.me_usages
	getUsages (FunctionEntry fe) = fe.fe_usages
	getUsages _                  = {}

	// Efficient union on sorted lists
	mergeUnion :: !['Database.Native'.Index] !['Database.Native'.Index] -> ['Database.Native'.Index]
	mergeUnion [] is = is
	mergeUnion is=:[_:_] [] = is
	mergeUnion orgis=:[i:is] orgjs=:[j:js]
	| i < j     = [i:mergeUnion is orgjs]
	| i > j     = [j:mergeUnion orgis js]
	| otherwise = [i:mergeUnion is js]

	mergeUnionWithAnnots :: ![('Database.Native'.Index,a)] ![('Database.Native'.Index,a)] -> [('Database.Native'.Index,a)]
	mergeUnionWithAnnots [] is = is
	mergeUnionWithAnnots is=:[_:_] [] = is
	mergeUnionWithAnnots orgis=:[a=:(i,_):is] orgjs=:[b=:(j,_):js]
	| i < j     = [a:mergeUnionWithAnnots is orgjs]
	| i > j     = [b:mergeUnionWithAnnots orgis js]
	| otherwise = [a:mergeUnionWithAnnots is js]

	// Efficient intersection on sorted lists
	mergeIntersect :: !['Database.Native'.Index] !['Database.Native'.Index] -> ['Database.Native'.Index]
	mergeIntersect [] is = []
	mergeIntersect is=:[_:_] [] = []
	mergeIntersect orgis=:['Database.Native'.Index i:is] orgjs=:['Database.Native'.Index j:js]
	| i < j     = mergeIntersect is orgjs
	| i > j     = mergeIntersect orgis js
	| otherwise = ['Database.Native'.Index i:mergeIntersect is js]

	collectUsages :: !['Database.Native'.Index] ![!CloogleEntry!] -> (!['Database.Native'.Index], !['Database.Native'.Index])
	collectUsages [i:is] [!e:es!]
	# (sorted,unsorted) = collectUsages is es
	= case e of
		FunctionEntry fe -> case fe.fe_typedef of
			?Just i -> (sorted, [i:unsorted])
			?None -> case fe.fe_class of
				?Just i -> (sorted, [i:unsorted])
				?None -> ([i:sorted], unsorted)
		_ -> ([i:sorted], unsorted)
	collectUsages [] [!!] = ([], [])
	collectUsages _ _ = abort "error in collectUsages\n"

	removeDupSorted :: !['Database.Native'.Index] -> ['Database.Native'.Index]
	removeDupSorted [x:xs] = [x:removeDupSorted (dropWhile ((==)x) xs)]
	removeDupSorted [] = []

allTypeSynonyms :: !*CloogleDB -> *(Map Name [TypeDef], !*CloogleDB)
allTypeSynonyms wrap=:{db,type_synonyms}
# (vals,db) = 'Database.Native'.getValuesByIndices` type_synonyms db
= (fromList
	$ map collect
	$ groupBy ((==) `on` fst)
	$ sortBy ((<) `on` fst)
	[(td.td_name, td) \\ TypeDefEntry {tde_typedef=td=:{td_rhs=TDRSynonym t}} <|- vals]
  , {wrap & db=db}
  )
where
	collect syns=:[(t,_):_] = (t,[s \\ (_,s) <- syns])
	collect []              = abort "internal error in allTypeSynonyms\n"

alwaysUniquePredicate :: !*CloogleDB -> *(!(String -> Bool), !*CloogleDB)
alwaysUniquePredicate wrap=:{always_unique} = (isJust o flip get always_unique, wrap)

getExactNameMatches :: !Name !*CloogleDB -> *(![!CloogleEntry!], !*CloogleDB)
getExactNameMatches name wrap=:{db,names}
# (values,db) = 'Database.Native'.getValuesByIndices` idxs db
= (values, {wrap & db=db})
where
	idxs = fromMaybe {} $ get name names

getInstances :: !Name !*CloogleDB -> *(![InstanceEntry], !*CloogleDB)
getInstances c wrap=:{db,instance_map}
| isNone idxs = ([], wrap)
# (vals,db) = 'Database.Native'.getValuesByIndices` (fromJust idxs) db
= ([ie \\ InstanceEntry ie <|- vals], {wrap & db=db})
where
	idxs = get c instance_map

getDerivations :: !Name !*CloogleDB -> *(![DeriveEntry], !*CloogleDB)
getDerivations c wrap=:{db,derive_map}
| isNone idxs = ([], wrap)
# (vals,db) = 'Database.Native'.getValuesByIndices` (fromJust idxs) db
= ([de \\ DeriveEntry de <|- vals], {wrap & db=db})
where
	idxs = get c derive_map

combineContainedEntries :: !*CloogleDB -> *CloogleDB
combineContainedEntries wrap=:{db}
# (es,db) = 'Database.Native'.getEntriesWithIndices db
= {wrap & db=Foldr remove db es}
where
	remove :: !(Index, !CloogleEntry, ![!Annotation!]) !*(NativeDB CloogleEntry Annotation)
		-> *NativeDB CloogleEntry Annotation
	remove (idx,e,annots) db = case e of
		FunctionEntry {fe_typedef = ?Just tdi} -> combineOrRemoveParentEntry tdi db
		FunctionEntry {fe_class = ?Just ci} -> combineOrRemoveParentEntry ci db
		_ -> db
	where
		combineOrRemoveParentEntry :: !Index !*(NativeDB CloogleEntry Annotation) -> *NativeDB CloogleEntry Annotation
		combineOrRemoveParentEntry parent_idx db
			# (inc,db) = 'Database.Native'.isIndexIncluded parent_idx db
			// Don't do anything if the parent definition is not included in the search results
			| not inc = db
			# (parent_annots,db) = 'Database.Native'.getAnnotationsByIndex parent_idx db
			// Don't do anything if the parent definition is a worse result
			| not (parent_annots betterNameWeightThan annots) = db
			# (parent,db) = 'Database.Native'.getValueByIndex parent_idx db
			// Remove the child entry
			# db = 'Database.Native'.unsearchIndex idx db
			// Update the annotations of the parent entry
			# new_parent_annots = updateAnnots annots parent_annots
			= 'Database.Native'.searchIndex parent_idx new_parent_annots db

		(betterNameWeightThan) x y = case weights x of
			[x:_] -> case weights y of
				[y:_] -> x > y
				_ -> True
			_ -> False
		where
			weights annots = [w \\ NameWeight w <|- annots]

		updateAnnots :: ![!Annotation!] ![!Annotation!] -> [!Annotation!]
		updateAnnots [!!] m = m
		updateAnnots [!NameWeight r:as!] m
			= updateAnnots as [!NameWeight $ maxList [r:match]:nomatch!]
		where
			(match,nomatch) = partition m

			partition :: [!Annotation!] -> (![Real], ![!Annotation!])
			partition [!!] = ([], [!!])
			partition [!NameWeight r:xs!] = let (yes,no) = partition xs in ([r:yes], no)
			partition [!x:xs!] = let (yes,no) = partition xs in (yes, [!x:no!])
		updateAnnots [!a=:Unifier _:as!]         m = updateAnnots as [!a:[!a \\ a <|- m | not (a=:Unifier _)!]!]
		updateAnnots [!a=:ExactResult:as!]       m = updateAnnots as [!a:[!a \\ a <|- m | not a=:ExactResult!]!]
		updateAnnots [!a=:UsedSynonyms _:as!]    m = updateAnnots as [!a:[!a \\ a <|- m | not (a=:UsedSynonyms _)!]!]
		updateAnnots [!a=:RequiredContext _:as!] m = updateAnnots as [!a:[!a \\ a <|- m | not (a=:RequiredContext _)!]!]

getEntries :: !*CloogleDB -> *(![!(CloogleEntry, [!Annotation!])!], !*CloogleDB)
getEntries wrap=:{db}
# (es,db) = 'Database.Native'.getEntries db
= (es, {wrap & db=db})

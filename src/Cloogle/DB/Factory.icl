implementation module Cloogle.DB.Factory

/**
 * Copyright 2016-2023 the authors (see README.md).
 *
 * This file is part of Cloogle.
 *
 * Cloogle is free software: you can redistribute it and/or modify it under the
 * terms of the GNU Affero General Public License as published by the Free
 * Software Foundation, version 3 of the License.
 *
 * Cloogle is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE.  See the GNU Affero General Public License for
 * more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with Cloogle. If not, see <https://www.gnu.org/licenses/>.
 *
 * The software is licensed under additional terms under section 7 of the GNU
 * Affero General Public License; see the LICENSE file for details.
 */

import StdArray
import StdBool
import StdDebug
import StdFile
from StdFunc import const, flip, id, o
import StdList
import StdOrdList
import StdOverloadedList
import StdString
import StdTuple

from Clean.Doc import :: ModuleDoc, :: FunctionDoc{vars,description}, :: ClassDoc,
	:: TypeDoc{fields,constructors}, :: ConstructorDoc, :: ClassMemberDoc,
	:: Description, :: ParseWarning(UsedReturn,IllegalField), :: ParseError,
	derive gDefault FunctionDoc TypeDoc,
	generic docBlockToDoc,
	derive docBlockToDoc ClassDoc ConstructorDoc FunctionDoc ModuleDoc TypeDoc,
	parseDoc, parseSingleLineDoc, :: DocBlock,
	class docType(..), instance docType FunctionDoc,
	class docConstructors(..), instance docConstructors TypeDoc,
	class docFields(..), instance docFields TypeDoc,
	traceParseError, traceParseWarnings,
	constructorToFunctionDoc, functionToClassMemberDoc, addClassMemberDoc
import Clean.Idents
import Clean.Parse
import Clean.Parse.Comments
import Clean.PrettyPrint
from Clean.Types import instance == Type
import qualified Clean.Types
from Clean.Types.Tree import :: TypeTree, instance zero (TypeTree v), addType
from Clean.Types.Unify import isomorphic_to
import qualified Clean.Types.Unify
from Clean.Types.Util import class print(print), instance print Type,
	instance print Priority, instance toString Type
import Clean.Types.CoclTransform
import Control.Applicative
import Control.Monad
import Data.Either
import Data.Error
from Data.Foldable import class Foldable(foldr1()), instance Foldable []
import Data.Func
import Data.Functor
import Data.GenDefault
import Data.List
from Data.Map import :: Map, instance Functor (Map k)
import qualified Data.Map
import Data.Maybe
import qualified Data.Set
from Data.Set import instance Foldable Set
import Data.Tuple
from Database.Native import :: NativeDB, :: Index(..), newDB,
	instance == Index, instance < Index
import qualified Database.Native
import System.Directory
import System.File
import System.FilePath
from Text import class Text(concat,indexOf,replaceSubString,startsWith),
	instance Text String, <+
import Text.GenJSON

from compile import :: DclCache{hash_table}, empty_cache
from Heap import :: Heap, newHeap, sreadPtr
from predef import init_identifiers
from syntax import
	:: AttrVarHeap, :: AttrVarHeapId, :: AttrVarInfo,
	:: BITVECT,
	:: ClassArgs(..),
	:: ClassDef{class_args,class_context,class_ident,class_pos},
	:: ExpressionHeap, :: ExpressionHeapId, :: ExprInfo,
	:: FileName, :: FunctionHeap, :: FunctionHeapId, :: FunctionInfo,
	:: FunctName, :: FunKind(FK_Macro), :: FunSpecials,
	:: GCF, :: GenericCaseDef{gc_gcf,gc_pos,gc_type},
	:: GenericCaseFunctions(GCF,GCFC),
	:: GenericDef{gen_ident,gen_pos,gen_type,gen_vars},
	:: GenericHeap, :: GenericHeapId, :: GenericInfo,
	:: Ident{id_name,id_info}, :: Import{import_module},
	:: KindHeap, :: KindHeapId, :: KindInfo,
	:: LineNr,
	:: Module{mod_defs,mod_ident},
	:: Optional(Yes,No), :: SymbolPtr, :: Ptr, :: SymbolTableEntry,
	:: ParsedConstructor{pc_cons_ident}, :: ParsedSelector{ps_field_ident},
	:: ParsedDefinition(PD_Class,PD_Derive,PD_Function,PD_Generic,PD_Instance,
		PD_Instances,PD_Type,PD_TypeSpec,PD_GenericCase,
		PD_NodeDef,PD_Import),
	:: ParsedExpr,
	:: ParsedImport,
	:: ParsedInstance{pi_ident,pi_pos,pi_types},
	:: ParsedInstanceAndMembers{pim_pi}, :: ParsedModule, :: ParsedTypeDef,
	:: Position(LinePos,NoPos), :: Priority, :: Rhs, :: ATypeVar,
	:: RhsDefsOfType(ConsList,ExtensibleConses,SelectorList,TypeSpec,EmptyRhs,
		AbstractTypeSpec,NewTypeCons,MoreConses),
	:: SymbolHeapId, :: SymbolTable, :: SymbolTableEntry, :: SymbolType,
	:: Type, :: TypeContext, :: TypeDef{td_ident,td_pos,td_rhs}, :: TypeVar,
	:: TypeVarHeap, :: TypeVarHeapId, :: TypeVarInfo,
	:: VarHeap, :: VarHeapId, :: VarInfo

import SPDX.License

from Cloogle.API import :: FunctionKind(..), :: ProblemResultExtras,
	instance == FunctionKind
import qualified Cloogle.DB
from Cloogle.DB import
	:: CloogleDB{..}, :: Annotation,
	:: Library,
	:: Location(Builtin,NoLocation),
	:: CleanLangReportLocation,
	:: CloogleEntry(..),
	:: ModuleEntry{..},
	:: FunctionEntry{..},
	:: TypeDefEntry{tde_loc,tde_instances,tde_derivations,tde_usages,tde_module_usages},
	:: ClassEntry{ce_loc,ce_instances,ce_is_meta,ce_members,ce_usages,ce_module_usages},
	classContext, :: TypeRestriction,
	:: SyntaxEntry,
	:: InstanceEntry{ie_class,ie_types,ie_locations},
	:: DeriveEntry{..},
	:: ABCInstructionEntry{..}, :: ABCArgument,
	instance zero FunctionEntry, instance zero ModuleEntry,
	class getLocation, instance getLocation CloogleEntry,
	instance == Location, instance < Location,
	location
from Cloogle.DB.FullText import :: FullTextIndex
import qualified Cloogle.DB.FullText

:: TemporaryDB =
	{ temp_functions         :: ![[('Cloogle.DB'.FunctionEntry, 'Data.Set'.Set String)]]
	, temp_classes           :: ![[('Cloogle.DB'.ClassEntry, [(String, 'Cloogle.DB'.FunctionEntry, 'Data.Set'.Set String)])]]
	, temp_instances         :: ![[('Cloogle.DB'.Name, [('Cloogle.DB'.Type, String)], 'Cloogle.DB'.Location)]]
	, temp_types             :: ![['Cloogle.DB'.TypeDefEntry]]
	, temp_derivations       :: ![[('Cloogle.DB'.Name, [('Cloogle.DB'.Type, String, 'Cloogle.DB'.Location)])]]
	, temp_class_derivations :: ![[('Cloogle.DB'.Name, 'Cloogle.DB'.Type, String, 'Cloogle.DB'.Location)]]
	, temp_modules           :: ![(ModuleEntry, 'Data.Set'.Set String, 'Data.Set'.Set String)]
		//* For modules we keep the imported modules in the dcl and the icl module separately
	, temp_licenses          :: !'Data.Map'.Map Library License
	}
	// TODO function usages in instances/derivations

newTemporaryDB :: ![IndexItem] -> TemporaryDB
newTemporaryDB index_items
	= { temp_functions         = []
	  , temp_classes           = []
	  , temp_instances         = []
	  , temp_types             = []
	  , temp_derivations       = []
	  , temp_class_derivations = []
	  , temp_modules           = []
	  , temp_licenses          = 'Data.Map'.fromList [(name, license) \\ {name,license} <- index_items]
	  }

finaliseDB :: ![CloogleEntry] !TemporaryDB -> *'Cloogle.DB'.CloogleDB
finaliseDB extra tdb =
	{ db = 'Database.Native'.mapInPlace link $ newDB entries
	, fulltext = 'Cloogle.DB.FullText'.finishFullTextIndex $ foldr (uncurry addName) 'Cloogle.DB.FullText'.newFullTextIndex entridxs
	, names = (\is->{i\\i<-is}) <$> foldr (\(name,i) -> flip 'Data.Map'.alter name \is -> case is of
			?None -> ?Just [i]
			?Just is -> ?Just [i:is]) 'Data.Map'.newMap
		[('Cloogle.DB'.getName loc, i) \\ (i,e) <- entridxs, ?Just loc <- ['Cloogle.DB'.getLocation e]]
	, types = foldl (\tree (tl,tr,v) -> addType tl tr v tree) zero
		[
			( snd $ 'Clean.Types.Unify'.prepare_unification True alwaysUnique synonymmap t`
			, snd $ 'Clean.Types.Unify'.prepare_unification False alwaysUnique synonymmap t`
			, i
			)
		\\ (i,FunctionEntry fe) <- entridxs
		, ?Just t <- [fe.fe_type <|> (docType =<< fe.fe_documentation)]
		, let t` = 'Clean.Types'.removeTypeContexts t
		]
	, core = coreidxs
	, builtins = idxarr \e -> fromMaybe False ('Cloogle.DB'.isBuiltin <$> 'Cloogle.DB'.getLocation e)
	, syntax = idxarr \e -> e=:(SyntaxEntry _)
	, abc_instrs = idxarr \e -> e=:(ABCInstructionEntry _)
	, type_synonyms = idxarr \e -> case e of
		TypeDefEntry tde -> ('Clean.Types'.td_rhs ('Cloogle.DB'.getTypeDef tde))=:('Clean.Types'.TDRSynonym _)
		_                -> False
	, library_map = libmap
	, module_map = modmap
	, derive_map = 'Data.Map'.fromList
		$ map collect_snds
		$ groupBy ((==) `on` fst) $ sort
		[(de.de_generic, i) \\ (i,DeriveEntry de) <- entridxs]
	, instance_map = 'Data.Map'.fromList
		$ map collect_snds
		$ groupBy ((==) `on` fst) $ sort
		[(ie.ie_class, i) \\ (i,InstanceEntry ie) <- entridxs]
	, always_unique = always_unique
	, licenses = tdb.temp_licenses
	}
where
	collect_snds xys=:[(x,_):_] = (x,{#y \\ (_,y) <- xys})
	collect_snds [] = abort "collect_snds requires at least one element\n"

	link :: !Int !CloogleEntry -> CloogleEntry
	link i e = case e of
		TypeDefEntry tde -> TypeDefEntry
			{ tde
			& tde_instances=idxarr \e -> case e of
				InstanceEntry ie -> case name of
					"(->)" -> any (\t -> t=:('Clean.Types'.Func _ _ _) || t=:('Clean.Types'.Arrow _)) $ concatMap ('Clean.Types'.subtypes o fst) ie.ie_types
					_      -> or [t == name \\ 'Clean.Types'.Type t _ <- concatMap ('Clean.Types'.subtypes o fst) ie.ie_types]
				_                -> False
			, tde_derivations=idxarr \e -> case e of
				DeriveEntry {de_type='Clean.Types'.Type t _}   -> t == name
				DeriveEntry {de_type='Clean.Types'.Arrow _}    -> name == "(->)"
				DeriveEntry {de_type='Clean.Types'.Func _ _ _} -> name == "(->)"
				_                                    -> False
			, tde_usages=fromMaybe {} ('Data.Map'.get name type_usages_map)
			, tde_module_usages=computeModuleUsages tde.tde_loc
			}
			with name = 'Clean.Types'.td_name $ 'Cloogle.DB'.getTypeDef tde
		ClassEntry ce -> ClassEntry
			{ ce
			& ce_instances=idxarr \e -> case e of
				InstanceEntry ie -> ie.ie_class == name
				_                -> False
			, ce_members=idxarr \e -> case e of
				FunctionEntry fe -> fe.fe_class == ?Just (Index i)
				_                -> False
			, ce_usages=fromMaybe {} ('Data.Map'.get name class_usages_map)
			, ce_module_usages=computeModuleUsages ce.ce_loc
			}
			with name = 'Cloogle.DB'.getName ce.ce_loc
		FunctionEntry fe -> FunctionEntry
			{ fe
			& fe_derivations=case fe.fe_derivations of
				?None -> ?None
				?Just _ -> ?Just $ idxarr \e -> case e of
					DeriveEntry de -> de.de_generic == name
					_              -> False
			, fe_usages=fromMaybe {} ('Data.Map'.get name function_usages_map)
			, fe_module_usages=computeModuleUsages fe.fe_loc
			}
			with name = 'Cloogle.DB'.getName fe.fe_loc
		ModuleEntry me -> ModuleEntry
			{ me
			& me_usages=fromMaybe {} ('Data.Map'.get name module_usages_map)
			}
			with name = 'Cloogle.DB'.getName me.me_loc
		e -> e

	// Modules that are imported with `import StdEnv`:
	stdEnvModules = 'Data.Set'.delete "StdEnv" $ walk ('Data.Set'.singleton "StdEnv") 'Data.Set'.newSet
	where
		thisLibrary = [(mod, dcl_imports) \\ ({me_loc='Cloogle.DB'.Location "base-stdenv" _ mod _ _ _ _}, dcl_imports, _) <- tdb.temp_modules]

		walk unseen seen
			| 'Data.Set'.null unseen
				= seen
			# (next, unseen) = 'Data.Set'.deleteFindMin unseen
			# imports = fromMaybe 'Data.Set'.newSet $ lookup next thisLibrary
			# unseen = 'Data.Set'.union unseen ('Data.Set'.difference imports seen)
			# seen = 'Data.Set'.insert next seen
			= walk unseen seen

	computeModuleUsages loc =
		maybe 0 size
			(flip 'Data.Map'.get module_usages_map =<< 'Cloogle.DB'.getModule loc) +
		// Add counts for StdEnv, because StdEnv is often exported directly instead of individual modules
		if ('Cloogle.DB'.getLibrary loc == ?Just "base-stdenv" && 'Data.Set'.member moduleName stdEnvModules)
			(size (fromJust ('Data.Map'.get "StdEnv" module_usages_map)))
			0
	where
		moduleName = fromJust ('Cloogle.DB'.getModule loc)

	make_usage_map :: ([[(a,Index)]] -> 'Data.Map'.Map a {#Index}) | <, == a
	make_usage_map = 'Data.Map'.fromList
		o map collect_snds
		o groupBy ((==) `on` fst)
		o sortBy ((<) `on` fst)
		o flatten

	type_usages_map = make_usage_map
		[[(subt,idx) \\ t <- usingTypes e, 'Clean.Types'.Type subt _ <- 'Clean.Types'.subtypes t] \\ (idx,e) <- entridxs]
	where
		usingTypes :: !CloogleEntry -> ['Clean.Types'.Type]
		usingTypes (FunctionEntry {fe_type = ?Just t}) = [t]
		usingTypes (TypeDefEntry tde) = typesOfRhs ('Clean.Types'.td_rhs ('Cloogle.DB'.getTypeDef tde))
		where
			typesOfRhs rhs = case rhs of
				'Clean.Types'.TDRSynonym t -> [t]
				'Clean.Types'.TDRAbstract (?Just rhs) -> typesOfRhs rhs
				'Clean.Types'.TDRAbstractSynonym t -> [t]
				_ -> [] // constructors and selectors are taken care of by FunctionEntry
		usingTypes _ = []

	class_usages_map = make_usage_map
		[[(cls,idx) \\ 'Clean.Types'.Instance cls _ <- context e] \\ (idx,e) <- entridxs]
	where
		context :: 'Cloogle.DB'.CloogleEntry -> ['Clean.Types'.TypeRestriction]
		context (FunctionEntry {fe_type = ?Just t}) = 'Clean.Types'.allRestrictions t
		context (TypeDefEntry tde) = 'Clean.Types'.typeRhsRestrictions $ 'Clean.Types'.td_rhs $ 'Cloogle.DB'.getTypeDef tde
		context (ClassEntry ce) = classContext ce
		context _ = []

	function_usages_map = make_usage_map
		[[(g,idx) \\ g <- 'Data.Set'.toList globs]
			\\ idx <- fidxs
			 & (fe,globs) <- [(fe, 'Data.Set'.newSet) \\ FunctionEntry fe <- extra] ++ function_entries]
	where fidxs = [idx \\ (idx,FunctionEntry _) <- entridxs]

	module_usages_map = make_usage_map
		[ [(i,idx) \\ i <- 'Data.Set'.toList imports]
		\\ idx <- midxs
		& (_, dcl_imports, icl_imports) <- tdb.temp_modules
		, let imports = 'Data.Set'.union dcl_imports icl_imports
		]
	where midxs = [idx \\ (idx,ModuleEntry _) <- entridxs]

	function_entries = flatten tdb.temp_functions ++
		[({ fun
		& fe_kind  = case fun.fe_kind of
			Function -> ClassMember
			Macro    -> ClassMacro
			_        -> abort "error while transforming class members to function entries\n"
		, fe_loc   = 'Cloogle.DB'.setName fname cls.ce_loc
		, fe_class = ?Just $ idxhd \ce -> case ce of
			ClassEntry ce -> ce.ce_loc == cls.ce_loc
			_             -> False
		}, ids) \\ clss <- tdb.temp_classes, (cls,funs) <- clss, (fname,fun,ids) <- funs] ++
		[({ f
		& fe_typedef = ?Just $ idxhd \tde -> case tde of
			TypeDefEntry tde -> tde.tde_loc == td.tde_loc
			_                -> False
		}, 'Data.Set'.newSet) \\ tds <- tdb.temp_types, td <- tds, f <- constructor_functions td ++ record_functions td]

	entries = [e \\ Right e <- entries`]
	entries` = map Right (
		extra ++
		[TypeDefEntry tde \\ tds <- tdb.temp_types, tde <- tds] ++
		[ModuleEntry mod \\ (mod,_,_) <- tdb.temp_modules] ++
		map ClassEntry classes ++
		map (FunctionEntry o fst) function_entries ++
		// Normal instances
		[InstanceEntry {ie_class=cls,ie_types=types,ie_locations=map thd3 is}
			\\ is=:[(cls,types,_):_] <- groupBy instanceEq
				$ sortBy ((<) `on` (\(c,ts,_) -> (c,map snd ts)))
				$ flatten tdb.temp_instances] ++
		// Derivations
		[DeriveEntry {de_generic=gn, de_type=t, de_type_representation=tr, de_locations=map fth4 ds}
			\\ ds=:[(gn,t,tr,_):_] <- groupBy ((==) `on` (\(g,_,tr,_) -> (g,tr)))
				$ sortBy ((<) `on` (\(g,_,tr,_) -> (g,tr)))
				[(g,t,tr,l) \\ ds <- tdb.temp_derivations, (g,ts) <- ds, (t,tr,l) <- ts]] ++
		[InstanceEntry {ie_class=gn, ie_types=[(t,tr)], ie_locations=map fth4 ds}
			\\ ds=:[(gn,t,tr,_):_] <- groupBy ((==) `on` (\(g,_,tr,_) -> (g,tr)))
				$ sortBy ((<) `on` (\(g,_,tr,_) -> (g,tr)))
				$ flatten tdb.temp_class_derivations]) ++
		// Meta-class instances
		concatMap metainstances classes
	where
		metainstances :: ClassEntry -> [Either String CloogleEntry]
		metainstances {ce_is_meta=False} = []
		metainstances ce = [Left cls:[Right $ InstanceEntry {ie_class=cls,ie_types=[(t,tr)],ie_locations=locs} \\ (t,tr,locs) <- types`]]
		where
			crestrs = [c \\ 'Clean.Types'.Instance c ['Clean.Types'.Var _] <- classContext ce | c <> "TC"] // TC class is implicit
			grestrs = [g \\ 'Clean.Types'.Derivation g ('Clean.Types'.Var _) <- classContext ce]
			ctypes = [[(hd ie.ie_types,ie.ie_locations) \\ InstanceEntry ie <- entries | ie.ie_class == c && length ie.ie_types == 1] \\ c <- crestrs]
			gtypes = [[((de.de_type,de.de_type_representation),de.de_locations) \\ DeriveEntry de <- entries | de.de_generic == g] \\ g <- grestrs]
			types = case ctypes ++ gtypes of
				[] -> []
				ts -> foldr1 intersect $ map (map fst) ts
			types` = [(t,tr,flatten [locs \\ rts <- ctypes ++ gtypes, ((t`,_),locs) <- rts | t == t`]) \\ (t,tr) <- types]
			cls = 'Cloogle.DB'.getName ce.ce_loc

			// Ideally we would use entries, but that causes a cycle in spine.
			// So we use entries up to the point where the class itself is defined.
			// This requires keeping an Either where Left holds the class under evaluation.
			// It also requires sorting the classes, see below with contextOrd.
			entries = [e \\ Right e <- takeWhile (\e -> case e of
				Left c  -> c <> cls
				Right _ -> True) entries`]

		classes = sortBy contextOrd [cls \\ clss <- tdb.temp_classes, (cls,_) <- clss]
		where
			contextOrd :: ClassEntry ClassEntry -> Bool
			contextOrd a b = isMember ('Cloogle.DB'.getName a.ce_loc) [c \\ 'Clean.Types'.Instance c _ <- classContext b]

		instanceEq :: (String, [('Cloogle.DB'.Type, a)], b) (String, [('Cloogle.DB'.Type, a)], b) -> Bool
		instanceEq (s, ts, _) (s2, ts2, _) = s == s2 && all (uncurry (isomorphic_to)) (zip2 (map fst ts) (map fst ts2))

	entridxs = zip2 [Index i \\ i <- [0..]] entries
	idxfilter f = [idx \\ (idx,e) <- entridxs | f e]
	idxarr f = {idx \\ idx <- idxfilter f}
	idxhd = hd o idxfilter

	coreidxs = idxarr \e -> case 'Cloogle.DB'.getLocation e of
		?None -> False
		?Just l -> case ('Cloogle.DB'.getLibrary l, 'Cloogle.DB'.getModule l) of
			(?Just l, ?Just m) -> isMember (l,m) coremods
			_                  -> False
	where
		coremods = [(fromJust $ 'Cloogle.DB'.getLibrary me.me_loc, fromJust $ 'Cloogle.DB'.getModule me.me_loc) \\ (me,_,_) <- tdb.temp_modules | me.me_is_core]

	libmap = 'Data.Map'.fromList
		[(l,idxarr \e -> case 'Cloogle.DB'.getLocation e >>= 'Cloogle.DB'.getLibrary of
			?None    -> False
			?Just l` -> l == l`) \\ l <- libs]
	where libs = removeDup [fromJust ('Cloogle.DB'.getLibrary me.me_loc) \\ (me,_,_) <- tdb.temp_modules]
	modmap = 'Data.Map'.fromList
		[(m,idxarr \e -> case 'Cloogle.DB'.getLocation e >>= 'Cloogle.DB'.getModule of
			?None    -> False
			?Just m` -> m == m`) \\ m <- mods]
	where mods = removeDup [fromJust ('Cloogle.DB'.getModule me.me_loc) \\ (me,_,_) <- tdb.temp_modules]

	synonymmap = 'Data.Map'.fromList
		$ map (appSnd (\is->[i\\i<-:is]) o collect_snds)
		$ groupBy ((==) `on` fst)
		$ sortBy ((<) `on` fst)
		[let td = 'Cloogle.DB'.getTypeDef tde in ('Clean.Types'.td_name td, td)
			\\ TypeDefEntry tde <- entries
			| ('Clean.Types'.td_rhs ('Cloogle.DB'.getTypeDef tde))=:('Clean.Types'.TDRSynonym _)]

	always_unique = 'Data.Map'.fromList
		[('Clean.Types'.td_name $ 'Cloogle.DB'.getTypeDef tde, ()) \\ TypeDefEntry tde <- entries | 'Clean.Types'.td_uniq $ 'Cloogle.DB'.getTypeDef tde]
	alwaysUnique = isJust o flip 'Data.Map'.get always_unique

	addName i entry idx = case 'Cloogle.DB.FullText'.index entry i idx of
		Error e -> abort (e +++ "\n")
		Ok idx -> idx

findModules :: !String !IndexItem !String !*World -> *(!['Cloogle.DB'.ModuleEntry], !*World)
findModules root item base w
#! (mbMetadata, w) = readFile (root </?> item.IndexItem.name </> ".nitrile.json") w
#! coreModules = fromMaybe 'Data.Set'.newSet (getCoreModules =<< fromJSON o fromString =<< error2mb mbMetadata)
#! (fps, w)   = readDirectory fullpath w
| isError fps = ([], w)
#! (Ok fps)   = fps
#! mods       = [makeEntry fn (isMember (replaceSubString ".icl" ".dcl" fn) fps) coreModules \\ fn <- fps | isIclModule fn]
#! (moremodss,w) = mapSt (findModules root item o ((+++) basedot)) (filter isDirectory fps) w
= (removeDupBy (\m -> 'Cloogle.DB'.getName m.me_loc) (mods ++ flatten moremodss), w)
where
	basedot = if (base == "") "" (base +++ ".")
	path = replaceSubString "." {pathSeparator} base
	fullpath = root </?> item.IndexItem.name </> "lib" </?> path

	(</?>) infixr 5 :: !FilePath !FilePath -> FilePath
	(</?>) "" p  = p
	(</?>) p  "" = p
	(</?>) p1 p2 = p1 </> p2

	getCoreModules :: !JSONNode -> ?('Data.Set'.Set String)
	getCoreModules (JSONObject keys) =
		lookup "fileinfo" keys >>= \fileinfo ->
		case fileinfo of
			JSONObject fileinfo ->
				?Just $ 'Data.Set'.fromList
					[ replaceSubString "/" "." (file % (4, size file-5)) // drop lib/ and extension
					\\ (file, info) <- fileinfo
					| startsWith "lib/" file && case info of
						JSONObject info -> lookup "core_module" info == ?Just (JSONBool True)
						_ -> False
					]
			_ ->
				?None
	getCoreModules _ = ?None

	makeEntry :: !String !Bool !('Data.Set'.Set String) -> 'Cloogle.DB'.ModuleEntry
	makeEntry fn has_dcl core_modules =
		{ me_loc           = location item.IndexItem.name item.version modname (path </?> fn) (if has_dcl (?Just 1) ?None) (?Just 1) modname
		, me_is_core       = 'Data.Set'.member modname core_modules
		, me_documentation = ?None
		, me_usages        = {}
		}
	where
		modname = basedot +++ fn % (0, size fn - 5)

	isIclModule :: String -> Bool
	isIclModule s = s % (size s - 4, size s - 1) == ".icl"

	isDirectory :: (String -> Bool)
	isDirectory = not o isMember '.' o fromString

	removeDupBy :: (a -> b) [a] -> [a] | Eq b
	removeDupBy f [x:xs] = [x:removeDupBy f (filter ((<>) (f x) o f) xs)]
	removeDupBy _ []     = []

indexModule :: !Bool !Bool !Bool !String !'Cloogle.DB'.ModuleEntry !TemporaryDB !*World
	-> *(!TemporaryDB, !*World)
indexModule trace_warnings trace_errors include_locals root mod db w
#! (functions,macros,generics,typedefs,clss,insts,derivs,clsderivs,(modname,mod`,dclimports,iclimports),w) =
	findModuleContents
		trace_warnings trace_errors
		include_locals
		(root </> lib </> "lib" </> mkdir ('Cloogle.DB'.getName mod.me_loc))
		w
#! typedefs = [{td & tde_loc=castLoc modname loc} \\ (loc,td) <- typedefs]
#! mod & me_loc = 'Cloogle.DB'.setModule modname ('Cloogle.DB'.setName modname mod.me_loc)
   mod & me_documentation = mod`.me_documentation
#! db  =
	{ db
	& temp_functions =
		[ [({f & fe_loc=castLoc modname loc},idents) \\ (loc,f,idents) <- functions ++ macros ++ generics]
		: db.temp_functions
		]
	, temp_classes = [[({ce & ce_loc=castLoc modname loc}, fs) \\ (loc,ce,fs) <- clss]:db.temp_classes]
	, temp_types = [typedefs:db.temp_types]
	, temp_instances = [castLocThd3 modname insts:db.temp_instances]
	, temp_derivations = [map (appSnd (castLocThd3 modname)) derivs:db.temp_derivations]
	, temp_class_derivations = [castLocFrth modname clsderivs:db.temp_class_derivations]
	, temp_modules = [(mod,dclimports,iclimports):db.temp_modules]
	}
= (db,w)
where
	lib = fromJust ('Cloogle.DB'.getLibrary mod.me_loc)
	version = fromJust ('Cloogle.DB'.getVersion mod.me_loc)

	castLocThd3 :: String -> ([(a, b, LocationInModule)] -> [(a, b, 'Cloogle.DB'.Location)])
	castLocThd3 m = map (appThd3 (castLoc m))
	castLocFrth m = map (\(a,b,c,l) -> (a,b,c,castLoc m l))

	castLoc :: String LocationInModule -> 'Cloogle.DB'.Location
	castLoc m l = location lib version m iclpath l.dcl_line l.icl_line $ fromMaybe "" l.LocationInModule.name
	iclpath = mkdir ('Cloogle.DB'.getName mod.me_loc) +++ ".icl"

	mkdir :: String -> String
	mkdir s = { if (c == '.') '/' c \\ c <-: s }

instance zero LocationInModule
where zero = {dcl_line = ?None, icl_line = ?None, name = ?None}

findModuleContents :: !Bool !Bool !Bool !String !*World
	-> *( ![(LocationInModule, 'Cloogle.DB'.FunctionEntry, 'Data.Set'.Set String)]
	    , ![(LocationInModule, 'Cloogle.DB'.FunctionEntry, 'Data.Set'.Set String)]
	    , ![(LocationInModule, 'Cloogle.DB'.FunctionEntry, 'Data.Set'.Set String)]
	    , ![(LocationInModule, 'Cloogle.DB'.TypeDefEntry)]
	    , ![(LocationInModule, 'Cloogle.DB'.ClassEntry, [(String, 'Cloogle.DB'.FunctionEntry, 'Data.Set'.Set String)])]
	    , ![('Cloogle.DB'.Name, [('Cloogle.DB'.Type, String)], LocationInModule)]
	    , ![('Cloogle.DB'.Name, [('Cloogle.DB'.Type, String, LocationInModule)])]
	    , ![('Cloogle.DB'.Name, 'Cloogle.DB'.Type, String, LocationInModule)]
	    , !('Cloogle.DB'.Name, 'Cloogle.DB'.ModuleEntry, 'Data.Set'.Set String, 'Data.Set'.Set String)
	    , !*World
	    )
findModuleContents trace_warnings trace_errors include_locals path w
#! (dclcomments,w) = scanComments (path +++ ".dcl") w
#! (dcl,w) = readModule (path +++ ".dcl") w
#! (dclmod,dcl,documentation) = case dcl of
	Error _ -> (zero, [], emptyCollectedComments)
	Ok (dcl,_) -> case dclcomments of
		Error _ -> (zero, dcl.mod_defs, emptyCollectedComments)
		Ok comments -> let coll = collectComments comments dcl in
			( {zero & me_documentation=docParseResultToMaybe (const True) =<< parseDoc <$> getComment dcl coll}
			, dcl.mod_defs
			, coll
			)
#! (icl,w) = readModule (path +++ ".icl") w
#! (icl,modname) = case icl of
	Error _ -> ([], "")
	Ok (icl,syms) -> (icl.mod_defs, icl.mod_ident.id_name)
#! dclimports = 'Data.Set'.fromList [i.import_module.id_name \\ PD_Import is <- dcl, i <- is]
   iclimports = 'Data.Set'.fromList [i.import_module.id_name \\ PD_Import is <- icl, i <- is]
#! contents=:(functions,rules,generics,typedefs,clss,insts,derivs,clsderivs) =
	( combine cmpLocFst3  joinLocFstIds pd_typespecs    dcl documentation icl
	, combine cmpLocFst3  joinLocFstIds pd_rewriterules dcl documentation icl
	, combine cmpLocFst3  joinLocFstIds pd_generics     dcl documentation icl
	, combine cmpLocFst   joinTypeDefs  pd_types        dcl documentation icl
	, combine cmpLocFst3  joinLocFst3   pd_classes      dcl documentation icl
	, combine cmpInsts    joinInsts     pd_instances    dcl documentation icl
	, combineDerivs (pd_derivations True dcl) (pd_derivations False icl)
	, combine cmpClsDeriv joinClsDeriv pd_class_derivations dcl documentation icl
	)
#! (functions,rules,generics,typedefs,clss,insts,derivs,clsderivs) =
	if include_locals
	contents
	( filter (hasDcl o fst3)            functions
	, filter (hasDcl o fst3)            rules
	, filter (hasDcl o fst3)            generics
	, filter (hasDcl o fst)             typedefs
	, filter (hasDcl o fst3)            clss
	, filter (hasDcl o thd3)            insts
	, filter (not o isEmpty o snd) (map (appSnd (filter (hasDcl o thd3))) derivs)
	, filter (hasDcl o (\(_,_,_,x)->x)) clsderivs
	) with hasDcl loc = isJust loc.dcl_line
#! rules = filter (\(r,_,_) -> not $ any (\(l,_,_)->fromJust l.LocationInModule.name == fromJust r.LocationInModule.name) functions) rules
= (functions,rules,generics,typedefs,clss,insts,derivs,clsderivs,(modname,dclmod,dclimports,iclimports),w)
where
	combine :: (a a -> Bool) (a a -> a)
		(Bool [ParsedDefinition] CollectedComments -> [a])
		![ParsedDefinition] !CollectedComments
		![ParsedDefinition]
		-> [a]
	combine eq join find dcl dclsym [] // Special case for things like _library.dcl
		= find True dcl dclsym
	combine eq join find dcl dclsym icl
		= unionBy eq join (find True dcl dclsym) (find False icl emptyCollectedComments)

	unionBy :: (a a -> Bool) (a a -> a) [a] [a] -> [a]
	unionBy eq join xs []     = xs
	unionBy eq join xs [y:ys] = case partition (eq y) xs of
		([],   xs) -> [y:unionBy eq join xs  ys]
		(found,xs) -> let (foundys,ys`) = partition (eq y) ys in
			[foldr join y (found ++ foundys):unionBy eq join xs ys`]

	cmpLoc x y = x.LocationInModule.name == y.LocationInModule.name

	cmpLocFst :: ((LocationInModule, a) (LocationInModule, a) -> Bool)
	cmpLocFst = cmpLoc `on` fst

	cmpLocFst3 :: ((LocationInModule, a, b) (LocationInModule, a, b) -> Bool)
	cmpLocFst3 = cmpLoc `on` fst3

	joinLocFst :: (LocationInModule, a) (LocationInModule, b) -> (LocationInModule, a)
	joinLocFst (l1,a) (l2,_) = (joinLoc l1 l2, a)

	joinLocFst3 :: (LocationInModule, a, b) (LocationInModule, c, d) -> (LocationInModule, a, b)
	joinLocFst3 (l1,a,b) (l2,_,_) = (joinLoc l1 l2, a, b)

	joinLocFstIds :: (LocationInModule, a, 'Data.Set'.Set String) (LocationInModule, b, 'Data.Set'.Set String) -> (LocationInModule, a, 'Data.Set'.Set String)
	joinLocFstIds (l1,a,idsa) (l2,_,idsb) = (joinLoc l1 l2, a, 'Data.Set'.union idsa idsb)

	joinTypeDefs :: (LocationInModule, 'Cloogle.DB'.TypeDefEntry) (LocationInModule, 'Cloogle.DB'.TypeDefEntry) -> (LocationInModule, 'Cloogle.DB'.TypeDefEntry)
	joinTypeDefs (a,t) (b,u) = (joinLoc a b, 'Cloogle.DB'.mergeTypeDefEntries t u)

	cmpInsts :: (a, b, LocationInModule) (a, b, LocationInModule) -> Bool | == a & == b
	cmpInsts (ca, tsa, _) (cb, tsb, _) = ca == cb && tsa == tsb
	joinInsts (c,ts,la) (_,_,lb) = (c,ts,joinLoc la lb)

	combineDerivs ::
		([('Cloogle.DB'.Name, [('Cloogle.DB'.Type, String, LocationInModule)])]
		 [('Cloogle.DB'.Name, [('Cloogle.DB'.Type, String, LocationInModule)])]
		 -> [('Cloogle.DB'.Name, [('Cloogle.DB'.Type, String, LocationInModule)])])
	combineDerivs = unionBy (on (==) fst) (\(n,ts) (_,us) -> (n,combineTypes ts us))
	where
		combineTypes = unionBy (on (==) fst3) (\(t,s,la) (_,_,lb) -> (t,s,joinLoc la lb))

	cmpClsDeriv :: (a, b, c, LocationInModule) (a, b, c, LocationInModule) -> Bool | == a & == b
	cmpClsDeriv (ca,ta,_,_) (cb,tb,_,_) = ca == cb && ta == tb
	joinClsDeriv (c,t,s,la) (_,_,_,lb) = (c,t,s,joinLoc la lb)

	joinLoc :: LocationInModule LocationInModule -> LocationInModule
	joinLoc a b =
		{ dcl_line = a.dcl_line              <|> b.dcl_line
		, icl_line = a.icl_line              <|> b.icl_line
		, name     = a.LocationInModule.name <|> b.LocationInModule.name
		}

	pd_rewriterules :: !Bool ![ParsedDefinition] !CollectedComments -> [(LocationInModule, 'Cloogle.DB'.FunctionEntry, 'Data.Set'.Set String)]
	pd_rewriterules dcl defs comments
		= [( setLine dcl pos {LocationInModule | zero & name = ?Just id.id_name}
		   , let infixts = findTypeSpec id defs
		         doc = findDoc hideIsUsedReturn pd comments <|> (flip (findDoc hideIsUsedReturn) comments =<< infixts)
		         type = (docType =<< doc) <|> pdType pd in
		     trace_type_warning id
		     { zero
		     & fe_kind           = Macro
		     , fe_type           = type
		     , fe_representation = ?Just $ signature id infixts type +++ cpp pd
		     , fe_priority       = findPrio id >>= 'Clean.Types'.toMaybePriority
		     , fe_documentation  = doc
		     }
		   , (idents ICExpression pd).globals
		   ) \\ pd=:(PD_Function pos id isinfix args rhs _) <- defs]
	where
		signature :: Ident (?ParsedDefinition) (?'Clean.Types'.Type) -> String
		signature _  ?None      ?None     = ""
		signature id ?None      (?Just t) = id.id_name <+ " :: " <+ t <+ "\n"
		signature _  (?Just pd) ?None     = cpp pd +++ "\n"
		signature _  (?Just pd) (?Just t) = cpp pd <+ " :: " <+ t <+ "\n"

		findPrio :: Ident -> ?Priority
		findPrio id = (\pd -> case pd of
			PD_TypeSpec _ _ p _ _ -> p
			_                     -> abort "error in findPrio\n") <$> findTypeSpec id defs

		findTypeSpec :: Ident [ParsedDefinition] -> ?ParsedDefinition
		findTypeSpec _  []          = ?None
		findTypeSpec id [pd=:(PD_TypeSpec _ id` prio _ _):defs]
		| id`.id_name == id.id_name = ?Just pd
		findTypeSpec id [_:defs]    = findTypeSpec id defs

		trace_type_warning :: !Ident !FunctionEntry -> FunctionEntry
		trace_type_warning id fe
		| not trace_warnings            = fe
		| isJust (findTypeSpec id defs) = fe
		| isJust fe.fe_type             = fe
		| otherwise                     = trace_n ("Doc warning: expected @type for '" +++ id.id_name +++ "'") fe

	pd_derivations :: !Bool ![ParsedDefinition] -> [('Cloogle.DB'.Name, [('Cloogle.DB'.Type, String, LocationInModule)])]
	pd_derivations dcl defs
		= [( id.id_name, [('Clean.Types'.toType gc_type, cpp gc_type, setLine dcl gc_pos zero)])
			\\ gcdefs <- [ds \\ PD_Derive ds <- defs] ++ [[d] \\ PD_GenericCase d _ <- defs]
			, {gc_type,gc_pos,gc_gcf=GCF id _} <- gcdefs]

	pd_generics :: !Bool ![ParsedDefinition] !CollectedComments -> [(LocationInModule, 'Cloogle.DB'.FunctionEntry, 'Data.Set'.Set String)]
	pd_generics dcl defs comments
		= [( setLine dcl gen_pos {LocationInModule | zero & name = ?Just id_name}
		   , { zero
		     & fe_type           = ?Just $ 'Clean.Types'.toType gen_type
		     , fe_generic_vars   = ?Just $ map 'Clean.Types'.toTypeVar gen_vars
		     , fe_representation = ?Just $ cpp gen
		     , fe_documentation  = findDoc hideIsUsedReturn gen comments
		     , fe_derivations    = ?Just {}
		     }
		   , 'Data.Set'.newSet
		   ) \\ gen=:(PD_Generic {gen_ident=id=:{id_name},gen_pos,gen_type,gen_vars}) <- defs]

	pd_typespecs :: !Bool ![ParsedDefinition] !CollectedComments -> [(LocationInModule, 'Cloogle.DB'.FunctionEntry, 'Data.Set'.Set String)]
	pd_typespecs dcl defs comments
		= [( setLine dcl pos {LocationInModule | zero & name = ?Just id_name}
		   , { zero
		     & fe_type           = ?Just $ 'Clean.Types'.toType t
		     , fe_priority       = 'Clean.Types'.toMaybePriority p
		     , fe_representation = ?Just $ cpp ts
		     , fe_documentation  = findDoc hideIsUsedReturn ts comments
		     }
		   , (idents ICExpression [pd \\ pd=:(PD_Function _ id _ _ _ _) <- defs | id.id_name == id_name]).globals
		   ) \\ ts=:(PD_TypeSpec pos id=:{id_name} p (Yes t) funspecs) <- defs]

	pd_class_derivations :: !Bool ![ParsedDefinition] CollectedComments -> [('Cloogle.DB'.Name, 'Cloogle.DB'.Type, String, LocationInModule)]
	pd_class_derivations dcl defs _
		= [(id.id_name, 'Clean.Types'.toType gc_type, cpp gc_type, setLine dcl gc_pos zero)
			\\ PD_Derive gcdefs <- defs, {gc_type,gc_pos,gc_gcf=GCFC id _} <- gcdefs]

	pd_instances :: !Bool ![ParsedDefinition] CollectedComments -> [('Cloogle.DB'.Name, [('Cloogle.DB'.Type, String)], LocationInModule)]
	pd_instances dcl defs _
		= [(id, types, setLine dcl pos zero) \\ (id,types,pos) <- instances]
	where
		instances = map (appSnd3 (map (\t -> ('Clean.Types'.toType t, cppp t)))) $
			[(i.pi_ident.id_name, i.pi_types, i.pi_pos) \\ PD_Instance {pim_pi=i} <- defs]
			++ [(i.pi_ident.id_name, i.pi_types, i.pi_pos) \\ PD_Instances pis <- defs, {pim_pi=i} <- pis]

	pd_classes :: !Bool ![ParsedDefinition] !CollectedComments -> [(LocationInModule, 'Cloogle.DB'.ClassEntry, [(String, 'Cloogle.DB'.FunctionEntry, 'Data.Set'.Set String)])]
	pd_classes dcl defs comments =
		[
			( setLine dcl class_pos {LocationInModule | zero & name = ?Just id_name}
			, 'Cloogle.DB'.toClass
				NoLocation
				(typeVarsOfClassArgs class_args)
				(all (\(_,fe,_) -> fe.fe_kind == Macro) members)
				('Clean.Types'.TypeContext
					[ tr
					\\ 'Clean.Types'.TypeContext tc <- map 'Clean.Types'.toTypeContext class_context
					, tr <- tc
					])
				(parseClassDoc typespecs pd comments)
			, members
			)
		\\ pd=:(PD_Class {class_ident=id=:{id_name},class_pos,class_args,class_context} clsdefs) <- defs
		, let
			typespecs = pd_typespecs True clsdefs comments
			macros = [(n,(r,ids)) \\ ({LocationInModule | name = ?Just n},{fe_representation = ?Just r},ids) <- pd_rewriterules dcl clsdefs comments]
			updateRepresentation n fe =
				{ fe
				& fe_kind=if (isNone $ lookup n macros) fe.fe_kind Macro
				, fe_representation=(fst <$> lookup n macros) <|> fe.fe_representation
				, fe_documentation=if (isSingleFunction typespecs id)
					((\d -> {FunctionDoc | d & vars=[]}) <$> findDoc hideIsUsedReturn pd comments)
					fe.fe_documentation
				}
			members = [(f,updateRepresentation f et,ids) \\ ({LocationInModule | name = ?Just f}, et, ids) <- typespecs]
		]
	where
		// When the class has one member with the same name as the class, use
		// the class documentation as the function's documentation. This is the
		// case for classes like `class zero a :: a`, which do not have a where
		// clause and hence no other place for the function's documentation.
		parseClassDoc :: [(LocationInModule, 'Cloogle.DB'.FunctionEntry, a)] ParsedDefinition !CollectedComments -> ?ClassDoc
		parseClassDoc members pd=:(PD_Class {class_ident=id} _) comments
		| isSingleFunction members id = flip addClassMemberDoc
			(functionToClassMemberDoc <$> findDoc hideIsUsedReturn pd comments)
			<$> findDoc hideFunctionOnClass pd comments
		| otherwise = flip (foldl addClassMemberDoc)
			[functionToClassMemberDoc <$> fe.fe_documentation \\ (_,fe,_) <- members]
			<$> findDoc hideIsUsedReturn pd comments
		parseClassDoc _ _ _ = abort "parseClassDoc called with non-PD_Class\n"

		isSingleFunction :: [(LocationInModule, 'Cloogle.DB'.FunctionEntry, a)] Ident -> Bool
		isSingleFunction members id = length members == 1
			&& fromJust (fst3 $ hd members).LocationInModule.name == id.id_name

		// Hide warnings about @result and @param on single function classes
		hideFunctionOnClass (IllegalField "param")  = False
		hideFunctionOnClass (IllegalField "result") = False
		hideFunctionOnClass w                       = hideIsUsedReturn w

		typeVarsOfClassArgs :: !ClassArgs -> ['Clean.Types'.Type]
		typeVarsOfClassArgs (ClassArg v rest) =
			[ 'Clean.Types'.Var ('Clean.Types'.toTypeVar v)
			: typeVarsOfClassArgs rest
			]
		typeVarsOfClassArgs (ClassArgPattern v args rest) =
			[ 'Clean.Types'.Cons ('Clean.Types'.toTypeVar v) ['Clean.Types'.toType a \\ a <- args]
			: typeVarsOfClassArgs rest
			]
		typeVarsOfClassArgs (ClassArgPatternSameTypeVar _ _) = abort "Cloogle.DB.Factory: unexpeced ClassArgPatternSameTypeVar\n"
		typeVarsOfClassArgs NoClassArgs = []

	pd_types :: !Bool ![ParsedDefinition] !CollectedComments -> [(LocationInModule, 'Cloogle.DB'.TypeDefEntry)]
	pd_types dcl defs comments
		= [let name = 'Clean.Types'.td_name td in
			( setLine dcl ptd.td_pos {LocationInModule | zero & name = ?Just name}
			, 'Cloogle.DB'.toTypeDefEntry NoLocation td $ ?Just $ findRhsDoc ptd $ fromMaybe gDefault{|*|} $
				findDoc (const True) pd comments
			) \\ pd=:(PD_Type ptd) <- defs
		  , let td = 'Clean.Types'.toTypeDef ptd
		  ]
	where
		findRhsDoc :: !ParsedTypeDef -> TypeDoc -> TypeDoc
		findRhsDoc {td_rhs=ConsList cs}           = addConses cs
		findRhsDoc {td_rhs=ExtensibleConses cs}   = addConses cs
		findRhsDoc {td_rhs=SelectorList _ _ _ fs} = addFields fs
		findRhsDoc _                              = id

		addFields :: ![ParsedSelector] !TypeDoc -> TypeDoc
		addFields [] doc = doc
		addFields [ps:fs] doc = {doc` & fields = ?Just [d:fromMaybe [] doc`.fields]}
		where
			doc` = addFields fs doc
			d = parseSingleLineDoc <$> getComment ps comments

		addConses :: ![ParsedConstructor] !TypeDoc -> TypeDoc
		addConses [] doc
			= {doc & constructors = ?Just []}
		addConses [pc:cs] doc = {doc` & constructors = ?Just [d:fromMaybe [] doc`.constructors]}
		where
			doc` = addConses cs doc
			d = docParseResultToMaybe (const True) =<< parseDoc <$> getComment pc comments

	toLine :: Position -> 'Cloogle.DB'.LineNr
	toLine (LinePos _ l) = ?Just l
	toLine _             = ?None

	docParseResultToMaybe :: (ParseWarning -> Bool)
		(Either ParseError (d, [ParseWarning])) -> ?d
	docParseResultToMaybe showw (Left e)
		| trace_errors = traceParseError e ?None
		| otherwise    = ?None
	docParseResultToMaybe showw (Right (doc,ws))
		| trace_warnings = traceParseWarnings (filter showw ws) (?Just doc)
		| otherwise      = ?Just doc

	hideIsUsedReturn :: ParseWarning -> Bool
	hideIsUsedReturn w = not $ isUsedReturn w

	findDoc :: (ParseWarning -> Bool) a CollectedComments -> ?d | docBlockToDoc{|*|} d & commentIndex a
	findDoc showw id coll = getComment id coll >>= \doc -> docParseResultToMaybe showw $ parseDoc doc

	isUsedReturn :: ParseWarning -> Bool
	isUsedReturn UsedReturn = True; isUsedReturn _ = False

	setLine :: !Bool !Position !LocationInModule -> LocationInModule
	setLine True  pos loc = {loc & dcl_line=toLine pos}
	setLine False pos loc = {loc & icl_line=toLine pos}

constructor_functions :: !'Cloogle.DB'.TypeDefEntry -> ['Cloogle.DB'.FunctionEntry]
constructor_functions etd = [
	{ zero
	& fe_loc            = 'Cloogle.DB'.setName c etd.tde_loc
	, fe_kind           = Constructor
	, fe_type           = ?Just f
	, fe_representation = ?Just $ concat $ print_prio c p ++ [" :: "] ++ print False f
	, fe_priority       = p
	, fe_documentation  = constructorToFunctionDoc <$> doc
	}
	\\ (c,f,p) <- 'Clean.Types'.constructorsToFunctions ('Cloogle.DB'.getTypeDef etd)
	 & doc <- cons_doc]
where
	print_prio :: !String (?'Clean.Types'.Priority) -> [String]
	print_prio name ?None     = [name]
	print_prio name (?Just p) = ["(",name,") ":print False p]

	cons_doc = fromMaybe [] (docConstructors =<< 'Cloogle.DB'.getTypeDefDoc etd) ++ repeat ?None

record_functions :: !'Cloogle.DB'.TypeDefEntry -> ['Cloogle.DB'.FunctionEntry]
record_functions etd = [
	{ zero
	& fe_loc            = 'Cloogle.DB'.setName f etd.tde_loc
	, fe_kind           = RecordField
	, fe_type           = ?Just t
	, fe_representation = ?Just $ concat [".", f, " :: ":print False t]
	, fe_documentation  = (\d -> {FunctionDoc | gDefault{|*|} & description = ?Just d}) <$> doc
	}
	\\ (f,t) <- 'Clean.Types'.selectorsToFunctions ('Cloogle.DB'.getTypeDef etd)
	 & doc <- field_doc]
where
	field_doc = fromMaybe [] (docFields =<< 'Cloogle.DB'.getTypeDefDoc etd) ++ repeat ?None

instance == (a,b,c,d) | == a & == b & == c & == d
where
	(==) (a,b,c,d) (p,q,r,s) = a == p && b == q && c == r && d == s

fth4 (a,b,c,d) :== d

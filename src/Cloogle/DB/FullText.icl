implementation module Cloogle.DB.FullText

/**
 * Copyright 2016-2023 the authors (see README.md).
 *
 * This file is part of Cloogle.
 *
 * Cloogle is free software: you can redistribute it and/or modify it under the
 * terms of the GNU Affero General Public License as published by the Free
 * Software Foundation, version 3 of the License.
 *
 * Cloogle is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE.  See the GNU Affero General Public License for
 * more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with Cloogle. If not, see <https://www.gnu.org/licenses/>.
 *
 * The software is licensed under additional terms under section 7 of the GNU
 * Affero General Public License; see the LICENSE file for details.
 */

import StdEnv

import Clean.Doc
import Clean.Types
import Control.Monad
import Data.Error
import Data.Func
import Data.Functor
import Data.List
import qualified Data.Map
from Data.Map import instance Functor (Map k)
import Data.Maybe
from Data.NGramIndex import :: NGramIndex, newNGramIndex
import qualified Data.NGramIndex
import Data.Tuple
import System.FilePath
import Text.GenJSON

import Snowball

import Cloogle.API
import Cloogle.DB
import Text.Stopwords
import Text.WordsFromCode

:: TempFullTextIndex v :== FullTextIndex v

:: FullTextIndex v =
	{ ngrams :: !NGramIndex v
	, stems :: !'Data.Map'.Map String (StemEntry v)
	}

:: StemEntry v =
	{ global_count :: !Real
	, entries :: !'Data.Map'.Map v Real
	}

derive JSONEncode Map
derive JSONDecode Map

derive JSONEncode FullTextIndex, NGramIndex, StemEntry
derive JSONDecode FullTextIndex, NGramIndex, StemEntry

snowball :: (String -> MaybeError String String)
snowball =: case createStemmer "english" of
	?Just stem -> \s -> case stem s of
		?|Just w -> Ok w
		?|None -> Error "Cloogle.DB.FullText: Snowball out of memory"
	?None -> \_ -> Error "Cloogle.DB.FullText: failed to create stemmer"

newFullTextIndex :: TempFullTextIndex v
newFullTextIndex =
	{ ngrams = newNGramIndex NGRAMS_N NGRAMS_CI
	, stems = 'Data.Map'.newMap
	}

index :: !CloogleEntry !v !(TempFullTextIndex v) -> MaybeError String (TempFullTextIndex v) | Eq, < v
// We never want to return InstanceEntries or DeriveEntries:
index (InstanceEntry _) _ idx = Ok idx
index (DeriveEntry _) _ idx = Ok idx
// ABCInstructionEntries are searched on using exact matches on the name; we don't need to index them:
index (ABCInstructionEntry _) _ idx = Ok idx
index e v idx=:{ngrams,stems}
	# mbStems = addStems e v stems
	  idx & stems = fromOk mbStems
	| isError mbStems = liftError mbStems
	| isRegularName name = Ok idx
	# idx & ngrams = 'Data.NGramIndex'.index name v ngrams
	= Ok idx
where
	name = case e of
		ABCInstructionEntry aie -> aie.aie_instruction
		CommonProblemEntry cpe -> cpe.problem_title
		e -> getName (fromJust (getLocation e))

	// NB: we use a 1000000x multiplier here to avoid very small numbers in the
	// database. Such small numbers are printed as, e.g., x.xxxe-05, and this
	// is not a valid format for Z3, causing problems in Cloogle.Search.Rank.
	addStems e v stems = foldr add stems <$> 'Data.Map'.toList <$> fmap ((*) 1000000.0) <$> elements False e
	where
		add (word, weight) stems = 'Data.Map'.alter (?Just o add` weight) word stems

		add` weight ?None =
			{ global_count = 1.0
			, entries = 'Data.Map'.singleton v weight
			}
		add` weight (?Just {global_count,entries}) =
			{ global_count = global_count + 1.0
			, entries = 'Data.Map'.alter (\w -> case w of
				?None -> ?Just weight
				?Just _ -> abort "should not happen\n") v entries
			}

finishFullTextIndex :: !(TempFullTextIndex v) -> FullTextIndex v
finishFullTextIndex idx = {idx & stems = modGlobalCount <$> idx.stems}
where
	modGlobalCount entry = {entry & global_count = log10 entry.global_count}

search :: !String !(FullTextIndex v) -> MaybeError String [(v, Real)] | Eq, Ord v
search s {ngrams,stems}
	| isRegularName s
		= collectStems <$> 'Data.Map'.keys <$> elements True s
		= Ok (map (appSnd toReal) $ 'Data.NGramIndex'.search s ngrams)
where
	collectStems words = 'Data.Map'.toList $ 'Data.Map'.unionsWith (*) $ map collect words

	collect word = case 'Data.Map'.get word stems of
		?None
			-> 'Data.Map'.newMap
		?Just {global_count,entries}
			-> 'Data.Map'.foldrWithKey` add 'Data.Map'.newMap entries
		with
			add v weight res
				# weight` = weight / global_count
				= 'Data.Map'.alter (?Just o maybe weight` ((+) weight`)) v res

isRegularName :: !String -> Bool
isRegularName s = and [isIdChar c \\ c <-: s] // NB: this does not check that the first character should be a letter
where
	isIdChar c = isAlphanum c || c == '_' || c == '`'
		|| c == '.' /* for hierarchical modules */
		|| c == ' ' /* queries may contain spaces */

hasRegularName :: !Location -> Bool
hasRegularName loc = isRegularName (getName loc)

/**
 * The below defines how much weights different parts of an entry get in the
 * fulltext index. There are two ways to build a fulltext entry.
 *
 * 1. With `combineElements`. This simply concatenates the texts found in each
 *    of the parts.
 * 2. With `weightedElements`. This allows you to set different weights for
 *    each part.
 *
 * It is important to note that `combineElements` is *NOT* the same as
 * `weightedElements` with equal weights for each part. When `weightedElements`
 * is used, factors for each word are multiplied by `weight/total_weight`. This
 * means that a word can never get full score unless it occurs in every part.
 *
 * A good use case for `combineElements` is `FunctionDoc`. There are several
 * parts here that will be empty for many entries (e.g., `@throws`
 * documentation). It should be possible for a word to get a high rank without
 * it occurring in `@throws`, suggesting that the weight for `@throws` should
 * be low. But if there is no documentation except for `@throws`, a word should
 * be able to get a high rank if it appears in `@throws`, which would require a
 * high weight. With `combineElements`, the different parts of the
 * documentation are simply seen as a single block of text, so that the weight
 * of the `@throws` blocks depends on how much other documentation there is.
 *
 * A good use case for `weightedElements` is `FunctionEntry`. In this case, the
 * name of a function should be very important, regardless of how much
 * documentation there is, or how large the representation of the function is.
 * Using `combineElements`, the name of a function with a lot of documentation
 * would not be important enough. With `weightedElements`, the function name
 * and documentation can be given fixed weights relative to each other, so that
 * this ratio does not depend on the amount of documentation.
 *
 * When using `weightedElements`, the best weights should be found by trial and
 * error.
 *
 * Both `combineElements` and `weightedElements` ensure that the sum of the
 * factors for all words does not exceed 1.
 */

class indexable a
where
	/**
	 * @param Whether stopwords are included. This is only used for `String`
	 *   and higher-order types like `?` and `[]`.
	 */
	elements :: !Bool !a -> MaybeError String (Map String Real)

combineElements :: ![MaybeError String (Map String Real)] -> MaybeError String (Map String Real)
combineElements xs = normalize <$> 'Data.Map'.unionsWith (+) <$> sequence xs
where
	normalize map = ((*) (1.0 / sum ('Data.Map'.elems map))) <$> map

:: Weighted = E.a: Weighted !Bool !Real !a & indexable a

weight :: !Int !a -> Weighted | indexable a
weight w v = Weighted False (toReal w) v

weightWithStopwords :: !Int !a -> Weighted | indexable a
weightWithStopwords w v = Weighted True (toReal w) v

weightedElements :: [Weighted] -> MaybeError String (Map String Real)
weightedElements xs = 'Data.Map'.unionsWith (+) <$> sequence
	[ fmap ((*) (weight / total_weight)) <$> elements withStopwords value
	\\ Weighted withStopwords weight value <- xs
	]
where
	total_weight = sum [w \\ Weighted _ w _ <- xs]

instance indexable (?a) | indexable a where elements b mb = maybe (Ok 'Data.Map'.newMap) (elements b) mb
instance indexable [a] | indexable a where elements b xs = combineElements (map (elements b) xs)

instance indexable String
where
	elements withStopwords s = combineElements
		[ flip 'Data.Map'.singleton 1.0 <$> snowball word
		\\ word <- wordsFromCode s
		| withStopwords || not (isStopword word)
		]

instance indexable CloogleEntry
where
	elements _ (FunctionEntry fe) = weightedElements
		[ weightWithStopwords (if (hasRegularName fe.fe_loc) 6 0) fe.fe_loc
		, weight 1 fe.fe_representation
		, weight 3 fe.fe_documentation
		]
	elements _ (TypeDefEntry tde) = weightedElements
		[ weightWithStopwords (if (hasRegularName tde.tde_loc) 7 0) tde.tde_loc
		, weight 3 tde.tde_doc
		]
	elements _ (ModuleEntry me) = weightedElements
		[ weightWithStopwords (if (hasRegularName me.me_loc) 7 0) me.me_loc
		, weight 3 me.me_documentation
		]
	elements _ (ClassEntry ce) = weightedElements
		[ weight (if (hasRegularName ce.ce_loc) 7 0) ce.ce_loc
		, weight 3 ce.ce_documentation
		]
	elements _ (SyntaxEntry se) = weightedElements
		[ weight 10 se.SyntaxEntry.syntax_title
		, weightWithStopwords 10 se.SyntaxEntry.syntax_code
		, weight 1 se.SyntaxEntry.syntax_description
		]
	elements _ (CommonProblemEntry cpe) = weightedElements
		[ weight 10 cpe.problem_title
		, weight 1 cpe.problem_description
		, weight 1 cpe.problem_solutions
		]
	elements _ _ =
		abort "Cloogle.DB.FullText: elements called on unknown CloogleEntry\n"

instance indexable Location
where
	elements _ (Location lib _ mod _ _ _ name) = weightedElements
		[ weight 1 lib
		, weight 1 mod
		, weightWithStopwords 10 name
		]
	elements _ (Builtin name _) = elements True name
	elements _ NoLocation = Ok 'Data.Map'.newMap

instance indexable ClassDoc
where
	elements _ doc = combineElements
		[ elements False (docDescription doc)
		, elements False (docVars doc)
		, elements False (docMembers doc)
		]

instance indexable ClassMemberDoc
where
	elements _ doc = combineElements
		[ elements False (docDescription doc)
		, elements False (docParams doc)
		, elements False (docResults doc)
		, elements False (docThrows doc)
		]

instance indexable ConstructorDoc
where
	elements _ doc = combineElements
		[ elements False (docDescription doc)
		, elements False (docParams doc)
		]

instance indexable FunctionDoc
where
	elements _ doc = combineElements
		[ elements False (docDescription doc)
		, elements False (docParams doc)
		, elements False (docVars doc)
		, elements False (docResults doc)
		, elements False (docThrows doc)
		]

instance indexable ModuleDoc
where
	elements _ doc =
		elements False (stripLicenseTexts <$> wordsFromCode <$> docDescription doc)
	where
		stripLicenseTexts [] = []
		stripLicenseTexts es = strip LICENSE_PATTERNS es

		strip [] [e:es] = [e:stripLicenseTexts es]
		strip [pattern:patterns] es
			# len = length pattern
			| take len es == pattern
				= stripLicenseTexts (drop len es)
				= strip patterns es

LICENSE_PATTERNS =: map wordsFromCode
	[ "This file is part of"
	, "is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, version 3 of the License."
	, "is free software: you can redistribute it and/or modify it under the terms of the GNU Affero General Public License as published by the Free Software Foundation, version 3 of the License."
	, "is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details."
	, "is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Affero General Public License for more details."
	, "You should have received a copy of the GNU General Public License along with"
	, "You should have received a copy of the GNU Affero General Public License along with"
	, "If not, see <https://www.gnu.org/licenses/>."
	, "The software is licensed under additional terms under section 7 of the GNU General Public License; see the LICENSE file for details."
	, "The software is licensed under additional terms under section 7 of the GNU Affero General Public License; see the LICENSE file for details."
	]

instance indexable ParamDoc
where
	elements _ doc = combineElements
		[ elements True doc.ParamDoc.name
		, elements False (docDescription doc)
		]

instance indexable TypeDoc
where
	elements _ doc = combineElements
		[ elements False (docDescription doc)
		, elements False (docVars doc)
		, elements False (docRepresentation doc)
		, elements False (docFields doc)
		, elements False (docConstructors doc)
		]

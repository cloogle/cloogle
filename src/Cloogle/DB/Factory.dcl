definition module Cloogle.DB.Factory

/**
 * Functions to populate a database using the compiler frontend.
 *
 * Copyright 2016-2023 the authors (see README.md).
 *
 * This file is part of Cloogle.
 *
 * Cloogle is free software: you can redistribute it and/or modify it under the
 * terms of the GNU Affero General Public License as published by the Free
 * Software Foundation, version 3 of the License.
 *
 * Cloogle is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE.  See the GNU Affero General Public License for
 * more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with Cloogle. If not, see <https://www.gnu.org/licenses/>.
 *
 * The software is licensed under additional terms under section 7 of the GNU
 * Affero General Public License; see the LICENSE file for details.
 */

from Data.Set import :: Set

from SPDX.License import :: License

import Cloogle.DB

:: TemporaryDB

newTemporaryDB :: ![IndexItem] ->TemporaryDB

finaliseDB :: ![CloogleEntry] !TemporaryDB -> *CloogleDB

/**
 * Something to index (typically, a library).
 */
:: IndexItem =
	{ name        :: !String         //* The human-readable name of the library
	, description :: !String         //* A human-readable description
	, license     :: !License        //* The license under which the library is published
	, url         :: !String         //* The URL of the GitLab project
	, version     :: !String         //* The library version
	, download    :: !String         //* The URL to download the latest version
	}

/**
 * Find all modules that could be indexed
 *
 * @param The root of the library directory (typically $CLEAN_HOME/lib).
 * @param The {{`IndexItem`}} to look in.
 * @param The base path to look in.
 * @param The World.
 * @result A list of modules found.
 */
findModules :: !String !IndexItem !String !*World -> *(![ModuleEntry], !*World)

/**
 * Update a database with all the information found in a module
 *
 * @param Whether to trace parse warnings to `stderr`.
 * @param Whether to trace parse errors to `stderr`.
 * @param Whether local definitions (that only exist in the icl) should be indexed.
 * @param The root of the library directory (typically $CLEAN_HOME/lib).
 * @param The module to index.
 * @param A function to update module information (to set me_is_core, me_is_app).
 * @param The old database.
 * @result The new database.
 */
indexModule :: !Bool !Bool !Bool !String !ModuleEntry !TemporaryDB !*World -> *(!TemporaryDB, !*World)

:: LocationInModule =
	{ dcl_line :: ! ?Int
	, icl_line :: ! ?Int
	, name     :: ! ?String
	}

/**
 * Parse a module and get its contents
 *
 * @param Whether to trace parse warnings to `stderr`.
 * @param Whether to trace parse errors to `stderr`.
 * @param Whether local definitions (that only exist in the icl) should be indexed.
 * @param The path to the module, without .icl or .dcl
 * @result Function type specifications
 * @result Macro definitions and other functions that do not have a type
 * @result Generic function definitions
 * @result Type definitions
 * @result Class definitions
 * @result Instances
 * @result Derivations
 * @result Class derivations
 * @result The module, with imported modules from the dcl and icl
 */
findModuleContents :: !Bool !Bool !Bool !String !*World
	-> *( ![(LocationInModule, FunctionEntry, Set String)]
	    , ![(LocationInModule, FunctionEntry, Set String)]
	    , ![(LocationInModule, FunctionEntry, Set String)]
	    , ![(LocationInModule, TypeDefEntry)]
	    , ![(LocationInModule, ClassEntry, [(String, FunctionEntry, Set String)])]
	    , ![(Name, [(Type, String)], LocationInModule)]
	    , ![(Name, [(Type, String, LocationInModule)])]
	    , ![(Name, Type, String, LocationInModule)]
	    , !(Name, ModuleEntry, Set String, Set String)
	    , !*World
	    )

/**
 * Transform the constructors of an algebraic data type into plain functions.
 */
constructor_functions :: !TypeDefEntry -> [FunctionEntry]

/**
 * Transform the record fields of a record type into plain functions.
 */
record_functions :: !TypeDefEntry -> [FunctionEntry]

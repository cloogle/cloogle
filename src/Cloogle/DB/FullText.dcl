definition module Cloogle.DB.FullText

/**
 * Copyright 2016-2023 the authors (see README.md).
 *
 * This file is part of Cloogle.
 *
 * Cloogle is free software: you can redistribute it and/or modify it under the
 * terms of the GNU Affero General Public License as published by the Free
 * Software Foundation, version 3 of the License.
 *
 * Cloogle is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE.  See the GNU Affero General Public License for
 * more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with Cloogle. If not, see <https://www.gnu.org/licenses/>.
 *
 * The software is licensed under additional terms under section 7 of the GNU
 * Affero General Public License; see the LICENSE file for details.
 */

from StdClass import class Eq, class Ord
from StdOverloaded import class ==, class <

from Data.Error import :: MaybeError
from Text.GenJSON import :: JSONNode, generic JSONEncode, generic JSONDecode

from Cloogle.DB import :: CloogleEntry

:: TempFullTextIndex v
:: FullTextIndex v

derive JSONEncode FullTextIndex
derive JSONDecode FullTextIndex

newFullTextIndex :: TempFullTextIndex v

index :: !CloogleEntry !v !(TempFullTextIndex v) -> MaybeError String (TempFullTextIndex v) | Eq, < v

finishFullTextIndex :: !(TempFullTextIndex v) -> FullTextIndex v

search :: !String !(FullTextIndex v) -> MaybeError String [(v, Real)] | Eq, Ord v

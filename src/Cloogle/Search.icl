implementation module Cloogle.Search

/**
 * Copyright 2016-2023 the authors (see README.md).
 *
 * This file is part of Cloogle.
 *
 * Cloogle is free software: you can redistribute it and/or modify it under the
 * terms of the GNU Affero General Public License as published by the Free
 * Software Foundation, version 3 of the License.
 *
 * Cloogle is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE.  See the GNU Affero General Public License for
 * more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with Cloogle. If not, see <https://www.gnu.org/licenses/>.
 *
 * The software is licensed under additional terms under section 7 of the GNU
 * Affero General Public License; see the LICENSE file for details.
 */

import StdArray
import StdBool
from StdFunc import const, id, flip, o
import StdList
import StdMisc
import StdOrdList
import StdOverloadedList
import StdString
import StdTuple

import Clean.Doc
import Clean.Types
import Clean.Types.Parse
import Clean.Types.Unify
import Clean.Types.Util
import Control.Applicative
import Control.Monad
import Data.Error
import qualified Data.Foldable
from Data.Foldable import class Foldable, instance Foldable ?
from Data.Func import $, on, `on`, mapSt
import Data.Functor
import Data.List
import qualified Data.Map
import Data.Maybe
import Data.Tuple
from Database.Native import :: Entry{value}
from Text import class Text(concat,indexOf,toLowerCase,split),
	instance Text String, concat3

import SPDX.License

import Cloogle.API
import Cloogle.DB
import Cloogle.Search.Rank

:: SearchStrategy
	= SSIdentity
	| SSName String
	| SSExactName String
	| SSTypes
	| SSClasses
	| SSUnify Type
	| SSUsing (*CloogleDB -> *CloogleDB) [String]
	| SSAnd SearchStrategy SearchStrategy

addStrategy :: (?SearchStrategy) SearchStrategy -> SearchStrategy
addStrategy ?None     strat = strat
addStrategy (?Just s) strat = SSAnd strat s

searchStrategy :: !SearchStrategy !*CloogleDB -> (!*CloogleDB, !?String)
searchStrategy SSIdentity      db = (db, ?None)
searchStrategy (SSName n)      db = filterName n db
searchStrategy (SSExactName n) db = (filterExactName n db, ?None)
searchStrategy SSTypes         db = (filterDB (\ce->ce=:(TypeDefEntry _)) db, ?None)
searchStrategy SSClasses       db = (filterDB (\ce->ce=:(ClassEntry _)) db, ?None)
searchStrategy (SSUnify t)     db = (filterUnifying t db, ?None)
searchStrategy (SSUsing f ns)  db = (filterUsages f ns db, ?None)
searchStrategy (SSAnd a b) db
	# (db,mbErr) = searchStrategy a db
	| isJust mbErr = (db,mbErr)
	| otherwise = searchStrategy b db

search :: !Request !*CloogleDB -> *(!MaybeError String [Result], !*CloogleDB)
search req cdb
# (mbErr,mbType,allsyns,usedsyns,entries,cdb) = search` req cdb
| isJust mbErr = (Error (fromJust mbErr), cdb)
# (rs,cdb) = mapSt (makeResult mbType allsyns usedsyns) [e \\ e <|- entries] cdb
= (Ok (map fst (sortBy distanceOrNrUsages [(r, e) \\ ?Just r <- rs & e <|- entries])), cdb)
where
	distanceOrNrUsages (lr, (le,_)) (rr, (re,_))
		# lr = getBasicResult lr
		# rr = getBasicResult rr
		| lr.distance < rr.distance = True
		| lr.distance > rr.distance = False
		| otherwise = size (usages le) < size (usages re)

search` :: !Request !*CloogleDB ->
	*(!?String
	, !?Type
	, !(Map String [TypeDef])
	, ![TypeDef]
	, ![!(CloogleEntry, [!Annotation!])!]
	, !*CloogleDB
	)
search` {unify,name,exactName,className,typeName,using,modules,libraries,page,include_builtins,include_core} cdb
# include_builtins = fromMaybe DEFAULT_INCLUDE_BUILTINS include_builtins
# include_core = fromMaybe DEFAULT_INCLUDE_CORE include_core
// Initial filters
# initfilter =
	if include_core id excludeCore o
	(case libraries of ?Just ls -> filterLibraries ls; ?None -> id) o
	(case modules   of ?Just ms -> filterModules   ms; ?None -> id) o
	if include_builtins includeBuiltins excludeBuiltins
# cdb = initfilter cdb
// Search strategy
# strat = SSIdentity
// Name search
# strat = addStrategy (SSName <$> name) strat
# strat = addStrategy (SSExactName <$> exactName) strat
# strat = addStrategy (flip SSAnd SSTypes <$> SSExactName <$> typeName) strat
# strat = addStrategy (flip SSAnd SSClasses <$> SSExactName <$> className) strat
// Unification search
# (allsyns,cdb) = allTypeSynonyms cdb
# (alwaysUnique,cdb) = alwaysUniquePredicate cdb
# mbPreppedType = prepare_unification True alwaysUnique allsyns <$> (unify >>= parseType o fromString)
# usedsyns = 'Data.Foldable'.concat (fst <$> mbPreppedType)
# mbType = snd <$> mbPreppedType
# strat = addStrategy (SSUnify <$> mbType) strat
// Usage search
# strat = addStrategy (SSUsing initfilter <$> using) strat
// Search and return results
# (cdb,mbErr) = searchStrategy strat cdb
| isJust mbErr = (mbErr,?None,'Data.Map'.newMap,[],[|],cdb)
# cdb = combineContainedEntries cdb
# (es,cdb) = getEntries cdb
= (?None,mbType,allsyns,usedsyns,es,cdb)

unifyInformation :: !(?Type) !(Map String [TypeDef]) ![TypeDef] !FunctionEntry !*CloogleDB
	-> *(!?Unifier, ![TypeDef], ![RequiredContext], !*CloogleDB)
unifyInformation orgsearchtype allsyns usedsyns fe db
| isNone orgsearchtype || isNone fe.fe_type = (?None, usedsyns, [], db)
# (alwaysUnique,db) = alwaysUniquePredicate db
# (usedsyns,fe_type) = appFst (flip (++) usedsyns) $ prep alwaysUnique $ fromJust fe.fe_type
# tvas = orgsearchtype >>= unify fe_type
| isNone tvas = (?None, usedsyns, [], db)
# tvas = fromJust tvas
# unif = finish_unification usedsyns tvas
// Required Context
# (ownContext,db) = ownContext fe db
# (required_context,db) = findContext ownContext fe_type tvas db
= (?Just unif,usedsyns,required_context,db)
where
	prep alwaysUnique = prepare_unification False alwaysUnique allsyns

	ownContext :: FunctionEntry *CloogleDB -> *([TypeRestriction], *CloogleDB)
	ownContext fe db
	| isJust fe.fe_generic_vars =
		([Derivation (getName fe.fe_loc) (snd $ prep (const False) $ Var v) \\ v <- fromJust fe.fe_generic_vars], db)
	= case fe.fe_class of
		?None    -> ([], db)
		?Just ci -> case getValueByIndex ci db of
			(ClassEntry ce,db) -> ([Instance (getName ce.ce_loc) (map (snd o prep (const False)) ce.ce_vars)], db)
			(_,db)             -> ([], db)

	findContext :: [TypeRestriction] Type [TVAssignment] *CloogleDB -> *([RequiredContext], *CloogleDB)
	findContext trs t tvas db
	# trs = removeDup (concatMap applyUnifToTR (getTC t ++ trs))
	= mapSt (\tr -> appFst (toRequiredContext tr) o findLocations tr) trs db
	where
		getTC :: Type -> [TypeRestriction]
		getTC t = case t of
			Func _ _ (TypeContext tc)
				-> tc
			Forall _ _ (TypeContext tc)
				-> tc
			_
				-> []

		applyUnifToTR :: TypeRestriction -> [TypeRestriction]
		applyUnifToTR (Instance c ts) = maybeToList $ Instance c <$> mapM uni ts
		applyUnifToTR (Derivation g t)
		| any isFunc subts = [Derivation g (Arrow ?None):derivs]
		| otherwise        = derivs
		where
			subts = [st \\ ut <- maybeToList (uni t), st <- subtypes ut]
			derivs` = [Derivation g (Type st []) \\ Type st _ <- subts]
			derivs = [Derivation g v \\ v=:(Var _) <- subts] ++ derivs`

		uni :: (Type -> ?Type)
		uni = fmap (remove_var_prefixes o norm) o assignAll tvas
		where
			remove_var_prefixes :: !Type -> Type
			remove_var_prefixes (Var v)         = Var (remove_prefix v)
			remove_var_prefixes (Cons c ts)     = Cons (remove_prefix c) (map remove_var_prefixes ts)
			remove_var_prefixes (Type t ts)     = Type t (map remove_var_prefixes ts)
			remove_var_prefixes (Func is r c)   = Func (map remove_var_prefixes is) (remove_var_prefixes r) c
			remove_var_prefixes (Uniq t)        = Uniq $ remove_var_prefixes t
			remove_var_prefixes (Forall vs t c) = Forall (map remove_var_prefixes vs) (remove_var_prefixes t) c
			remove_var_prefixes (Arrow mt)      = Arrow $ remove_var_prefixes <$> mt
			remove_var_prefixes (Strict t)      = Strict $ remove_var_prefixes t

			remove_prefix s = s % (start, size s-1)
			where
				start = if (s.[0] == '_') (if (isDigit s.[1]) 3 2) 1

		norm :: (Type -> Type)
		norm = snd o resolve_synonyms allsyns

		toRequiredContext :: !TypeRestriction ![Location] -> RequiredContext
		toRequiredContext tr locs =
			{ representation = concat $ print False tr
			, only_free_vars = onlyFreeVars tr
			, locations      = map locResult locs
			}
		where
			onlyFreeVars (Derivation _ t) = isVar t
			onlyFreeVars (Instance _  ts) = all onlyFreeVars` ts
			where
				onlyFreeVars` (Var _) = True
				onlyFreeVars` (Cons _ ts) = all onlyFreeVars` ts
				onlyFreeVars` _ = False

		findLocations :: TypeRestriction *CloogleDB -> *([Location], *CloogleDB)
		findLocations (Instance c ts) db
		# (ies,db) = getInstances c db
		= (removeDup $ flatten
			[ ie.ie_locations \\ ie <- ies
			| and [norm t1 generalises t2 \\ (t1,_) <- ie.ie_types & t2 <- ts]], db)
		findLocations (Derivation g t) db
		# (des,db) = getDerivations g db
		= (removeDup $ flatten
			[de.de_locations \\ de <- des | norm de.de_type generalises t], db)

makeResult :: !(?Type) !(Map String [TypeDef]) ![TypeDef]
	!(!CloogleEntry, ![!Annotation!]) !*CloogleDB
	-> *(! ?Result, !*CloogleDB)
makeResult orgsearchtype allsyns usedsyns (entry, annots) db=:{licenses} = case entry of
	FunctionEntry fe
		// Parent class
		# (cls,db) = case fe.fe_class of
			?None   -> (?None, db)
			?Just i -> case getValueByIndex i db of
				(ClassEntry ce, db) -> (?Just {cls_name=getName ce.ce_loc, cls_vars=map printClassArg ce.ce_vars}, db)
				(_, db)             -> (?None, db)
		// Unifier
		# (unif,usedsyns,required_context,db) = unifyInformation orgsearchtype allsyns usedsyns fe db
		# annots = [!RequiredContext required_context,UsedSynonyms (length usedsyns):annots!]
		# annots = case unif of
			?Just unif -> [!Unifier unif:annots!]
			?None      -> annots
		// Derivations
		# (derivs,db) = case fe.fe_derivations of
			?None    -> (?None, db)
			?Just ds -> appFst ?Just $ getValuesByIndices` ds db
		-> (?Just $ FunctionResult (
			{ general
			& distance = distance entry annots
			, documentation = docDescription =<< fe.fe_documentation
			},
			{ kind = fe.fe_kind
			, func = fromJust (fe.fe_representation <|> pure (concat $ print False (name,fe)))
			, unifier = toStrUnifier <$> unif
			, required_context = if (isEmpty required_context)
				?None
				(?Just (map (\{representation,locations} -> (representation, locations)) required_context))
			, cls = cls
			, constructor_of = case (fe.fe_kind,fe.fe_type) of
				(Constructor, ?Just (Func _ r _)) -> ?Just $ concat $ print False r
				_                                 -> ?None
			, recordfield_of = case (fe.fe_kind,fe.fe_type) of
				(RecordField, ?Just (Func [Strict t:_] _ _)) -> ?Just $ concat $ print False t
				_                                            -> ?None
			, generic_derivations = case derivs of
				?None    -> ?None
				?Just ds -> ?Just $ sortBy ((<) `on` fst)
					[case value of
						DeriveEntry de -> (de.de_type_representation, map locResult de.de_locations)
						_              -> abort "internal error in makeResult_FunctionEntry\n"
					\\ value <|- ds]
			, param_doc = map toString <$> docParams <$> fe.fe_documentation
			, generic_var_doc = docVars <$> fe.fe_documentation
			, result_doc = docResults <$> fe.fe_documentation
			, type_doc = concat <$> print False <$> (docType =<< fe.fe_documentation)
			, throws_doc = docThrows <$> fe.fe_documentation
			}), db)
		with
			toStrUnifier :: Unifier -> StrUnifier
			toStrUnifier unif =
				{ StrUnifier
				| left_to_right = [toStr v t \\ LeftToRight (v,t) <- unif.assignments]
				, right_to_left = [toStr v t \\ RightToLeft (v,t) <- unif.assignments]
				, used_synonyms = [
					( concat $ [td.td_name," ":intersperse " " $ print False td.td_args]
					, concat $ print False s)
					\\ td=:{td_rhs=TDRSynonym s} <- unif.Unifier.used_synonyms]
				}
			where
				toStr var type = (var, concat $ print False type)

	TypeDefEntry tde
		# (insts,db) = getValuesByIndices` tde.tde_instances db
		# (derivs,db) = getValuesByIndices` tde.tde_derivations db
		-> (?Just $ TypeResult (
			{ general
			& documentation = docDescription =<< tde.tde_doc
			},
			{ type = concat $ print False tde.tde_typedef
			, type_instances = sortBy ((<) `on` fst3)
				[(ie.ie_class, map snd ie.ie_types, map locResult ie.ie_locations) \\ InstanceEntry ie <|- insts]
			, type_derivations = sortBy ((<) `on` fst)
				[(de.de_generic, map locResult de.de_locations) \\ DeriveEntry de <|- derivs]
			, type_var_doc            = docVars <$> tde.tde_doc
			, type_field_doc          = docFields =<< tde.tde_doc
			, type_constructor_doc    = map ((=<<) docDescription) <$> (docConstructors =<< tde.tde_doc)
			, type_representation_doc = join (docRepresentation =<< tde.tde_doc)
			}), db)

	ModuleEntry me
		-> (?Just $ ModuleResult (
			{ general
			& documentation = docDescription =<< me.me_documentation
			},
			{ module_is_core = me.me_is_core
			}), db)

	ClassEntry ce=:{ce_context=TypeContext context}
		# (ies,db) = getValuesByIndices` ce.ce_instances db
		# (mems,db) = getValuesByIndices` ce.ce_members db
		-> (?Just $ ClassResult (
			{ general
			& documentation = docDescription =<< ce.ce_documentation
			},
			{ class_name = name
			, class_heading = concat
				[ foldl (\acc arg -> concat3 acc " " (printClassArg arg)) name ce.ce_vars
				, if (isEmpty context) "" " | "
				: print False (TypeContext context)
				]
			, class_funs = [fromJust fe.fe_representation \\ FunctionEntry fe <|- mems]
			, class_fun_doc = ?Just [printDoc <$> fe.fe_documentation \\ FunctionEntry fe <|- mems]
			, class_instances = sortBy ((<) `on` fst)
				[(map snd ie.ie_types, map locResult ie.ie_locations)
					\\ InstanceEntry ie <|- ies]
			}), db)

	SyntaxEntry se
		-> (?Just $ SyntaxResult (
			{ general
			& documentation = ?Just se.syntax_description
			},
			{ SyntaxResultExtras
			| syntax_title    = se.SyntaxEntry.syntax_title
			, syntax_code     = se.SyntaxEntry.syntax_code
			, syntax_examples = se.SyntaxEntry.syntax_examples
			}), db)

	ABCInstructionEntry aie
		-> (?Just $ ABCInstructionResult (
			{ general
			& documentation = ?Just aie.aie_description
			},
			{ abc_instruction = aie.aie_instruction
			, abc_arguments   = aie.aie_arguments
			}), db)

	CommonProblemEntry cpe
		-> (?Just $ ProblemResult (general, cpe), db)

	_ // InstanceEntry / DeriveEntry cannot be returned
		-> (?None, db)
where
	mbLoc = getLocation entry
	name = getName $ fromJust mbLoc
	general =
		{ library  = fromMaybe "" library
		, version  = getVersion =<< mbLoc
		, license  = licenseDetails <$> (flip 'Data.Map'.get licenses =<< library)
		, modul    = fromMaybe "" (getModule =<< mbLoc)
		, filename = fromMaybe "" (getFilename =<< mbLoc)
		, dcl_line = getDclLine =<< mbLoc
		, icl_line = getIclLine =<< mbLoc
		, name     = fromMaybe "" (getName <$> mbLoc)
		, distance = distance entry annots
		, builtin  = case mbLoc of
			?Just (Builtin _ _) -> ?Just True
			_                   -> ?None
		, documentation = ?None // Added after pattern match on Entry type
		, langrep_documentation = case mbLoc of
			?Just (Builtin _ d) -> ?Just d
			_                   -> ?None
		}
	where
		library = getLibrary =<< mbLoc

		licenseDetails license =
			{ id   = licenseId license
			, name = licenseName license
			}

	printClassArg :: !Type -> String
	printClassArg t = concat (print True t)

searchWithSuggestions :: !Request !*CloogleDB -> *(!MaybeError String [Result], ![(Request,[Result])], !*CloogleDB)
searchWithSuggestions req db
# (mbRes,db) = search req db
| isError mbRes = (mbRes, [], db)
# (suggs,db) = suggestions req (fromOk mbRes) db
= (mbRes,suggs,db)
where
	suggestions :: !Request ![Result] !*CloogleDB -> *([(Request, [Result])], *CloogleDB)
	suggestions {page = ?Just n} _ db | n > 0 = ([], db)
	suggestions orgreq orgresults db
	# (swapped, db) = swap db
	# (capitalized, db) = capitalize db
	# (withcore, db) = addcore db
	= (swapped ++ capitalized ++ withcore, db)
	where
		orgtype = orgreq.unify >>= parseType o fromString

		swap db = case orgtype of
			?Just (Func is r cc) | length is < 3
				-> appFst (\reqs -> [(req,res) \\ (req,Ok res) <- reqs | enough res]) $ mapSt (\r -> appFst (tuple r) o search r o resetDB) reqs db
				with
					reqs = [{orgreq & unify = ?Just $ concat $ print False $ Func is` r cc}
						\\ is` <- permutations is | is` <> is]
			_ -> ([], db)
		where
			orgresults_n = length orgresults
			enough res = length res > orgresults_n

		capitalize db = case t` of
			?Just t` | fromJust orgtype <> t`
				-> appFst (maybe [] (\res -> [(req,res)]) o error2mb) $ search req $ resetDB db
					with req = {orgreq & unify = ?Just $ concat $ print False t`}
			_
				-> ([], db)
		where
			t` = assignAll
				[ ("int",     Type "Int" [])
				, ("bool",    Type "Bool" [])
				, ("char",    Type "Char" [])
				, ("real",    Type "Real" [])
				, ("file",    Type "File" [])
				, ("string",  Type "String" [])
				, ("dynamic", Type "Dynamic" [])
				, ("world",   Uniq (Type "World" []))
				] =<< orgtype

		addcore db
		| isJust orgreq.unify = ([], db) // unification search can be slow
		| fromMaybe DEFAULT_INCLUDE_CORE orgreq.include_core == DEFAULT_INCLUDE_CORE
			# req = {orgreq & include_core = ?Just (not DEFAULT_INCLUDE_CORE)}
			# (mbRes,db) = search req $ resetDB db
			  res = fromOk mbRes
			| isError mbRes || isEmpty res = ([], db)
			| isEmpty orgresults = ([(req,res)], db)
			# orghddistance = (getBasicResult (hd orgresults)).distance
			# newhddistance = (getBasicResult (hd res)).distance
			| newhddistance < orghddistance
				= ([(req,res)], db)
				= ([], db)
		| otherwise = ([], db)

locResult :: Location -> LocationResult
locResult (Location lib v mod filename dcl icl _) = (lib,v,mod,filename,dcl,icl)
locResult _ = abort "locResult called for non-Location\n"

isModMatch :: ![String] Location -> Bool
isModMatch mods (Location _ _ mod _ _ _ _) = isMember mod mods
isModMatch _ (Builtin _ _) = False

isLibMatch :: ![String] Location -> Bool
isLibMatch libs (Location lib _ _ _ _ _ _) = isMember lib libs
isLibMatch _ (Builtin _ _) = True

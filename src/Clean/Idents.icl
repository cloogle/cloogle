implementation module Clean.Idents

/**
 * Copyright 2016-2023 the authors (see README.md).
 *
 * This file is part of Cloogle.
 *
 * Cloogle is free software: you can redistribute it and/or modify it under the
 * terms of the GNU Affero General Public License as published by the Free
 * Software Foundation, version 3 of the License.
 *
 * Cloogle is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE.  See the GNU Affero General Public License for
 * more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with Cloogle. If not, see <https://www.gnu.org/licenses/>.
 *
 * The software is licensed under additional terms under section 7 of the GNU
 * Affero General Public License; see the LICENSE file for details.
 */

from StdOverloadedList import Foldl

from Data.Func import $
from Data.List import concatMap
from Data.Set import :: Set, newSet, union, unions, difference, fromList, Foldl

import syntax

removeLocalGlobals :: Idents -> Idents
removeLocalGlobals ids = {ids & globals=difference ids.globals ids.locals}

noLocals :: Idents -> Idents
noLocals ids = {ids & locals=newSet}

instance zero Idents
where
	zero =
		{ locals        = newSet
		, globals       = newSet
		, record_fields = newSet
		}

instance + Idents
where
	(+) a b = removeLocalGlobals
		{ locals        = union a.locals        b.locals
		, globals       = union a.globals       b.globals
		, record_fields = union a.record_fields b.record_fields
		}

instance idents Idents where idents _ id = id

instance idents [t] | idents t
where
	idents c xs = removeLocalGlobals
		{ locals        = unions [x.locals        \\ x <- xids]
		, globals       = unions [x.globals       \\ x <- xids]
		, record_fields = unions [x.record_fields \\ x <- xids]
		}
	where xids = map (idents c) xs

instance idents (Optional a) | idents a
where
	idents _ No      = zero
	idents c (Yes x) = idents c x

instance idents String
where
	idents ICExpression s = {zero & globals=fromList [s]}
	idents ICPattern    s = {zero & locals= fromList [s]}

instance idents Ident where idents c id = idents c id.id_name

instance idents ParsedDefinition
where
	idents c pd = case pd of
		PD_Function _ id _ args rhs _ ->
			idents ICPattern id +
			noLocals (idents ICPattern args + idents ICExpression rhs)
		PD_NodeDef _ e rhs -> idents ICPattern e + idents ICExpression rhs
		PD_Type _ -> abort "idents PD_Type\n"
//		PD_Type ptd -> abort "idents PD_Type\n"
		PD_TypeSpec _ _ _ _ _ -> zero
		PD_LocalFunctionTypeSpec _ _ _ _ _ -> zero
		PD_Class _ _ -> abort "idents PD_Class\n"
//		PD_Class ClassDef [ParsedDefinition]
		PD_Instance _ -> abort "idents PD_Instance\n"
//		PD_Instance ParsedInstanceAndMembers
//		PD_Instances [ParsedInstanceAndMembers]
//		PD_Import [ParsedImport]
//		PD_ImportedObjects [ImportedObject]
//		PD_ForeignExport !Ident !{#Char} !Int !Bool /* if stdcall */
//		PD_Generic GenericDef
//	 	PD_GenericCase GenericCaseDef Ident
//		PD_Derive [GenericCaseDef]
//		PD_Documentation DocType String
//		PD_Erroneous
		_ -> abort "idents of unknown ParsedDefinition\n"

instance idents ParsedExpr
where
	idents c pe = case pe of
		PE_List pes -> idents c pes
		PE_Ident id -> idents c id
		PE_Basic b -> idents c b
		PE_Bound b -> src + idents c b.bind_dst + {zero & locals=src.record_fields}
			with src = idents c b.bind_src
		PE_Lambda _ args rhs _ -> noLocals (idents ICPattern args + idents ICExpression rhs)
		PE_Tuple es -> idents c es
		PE_Record init _ fields -> idents c init + idents c [f.bind_dst \\ f <- fields] + idents c [f.bind_src \\ f <- fields]
		PE_ArrayPattern eas -> foldr (\x -> (+) (idents c x.bind_src + idents c x.bind_dst)) zero eas
		PE_UpdateComprehension base (PE_Update _ sels new) _ qs ->
			noLocals (idents ICExpression [base,new] + idents ICExpression sels + idents ICPattern qs)
		PE_UpdateComprehension base (PE_Let locals (PE_Update _ sels new)) updateable qs ->
			noLocals (idents ICPattern updateable + idents ICExpression [base,new] + idents ICPattern locals + idents ICExpression sels + idents ICPattern qs)
		PE_ArrayDenot _ es -> idents c es
		PE_Selection _ e s -> idents c e + idents c s
		PE_Update b s e -> idents c b + idents c s + idents c e
		PE_Case _ pe alts -> idents c pe + idents c alts
		PE_If _ b t e -> idents c [b,t,e] + idents c "if"
		PE_Let locals e -> noLocals (idents ICPattern locals + idents ICExpression e)
		PE_ListCompr _ _ e qs -> noLocals (idents ICPattern qs + idents ICExpression e)
		PE_ArrayCompr _ e qs -> noLocals (idents ICPattern qs + idents ICExpression e)
		PE_Sequ s -> idents c s
		PE_WildCard -> zero
		PE_Matches _ e p _ -> idents ICPattern e + idents ICPattern p
		PE_QualifiedIdent _ s -> idents c s
		PE_ABC_Code _ _ -> zero
		PE_Any_Code _ _ _ -> zero
		PE_DynamicPattern pe _ -> idents c pe
		PE_Dynamic pe _ -> idents c pe + idents c "dynamic"
		PE_Generic id _ -> idents c id
		PE_ExpressionType _ e -> idents c e
		PE_TypeSignature _ e -> idents c e
		PE_Empty -> zero
		_ -> abort "idents ParsedExpr\n"

instance idents BasicValue
where
	idents _ b = case b of
		BVB b -> idents ICExpression (toString b)
		_     -> zero

instance idents Rhs
where
	idents ICExpression rhs = noLocals (idents ICExpression rhs.rhs_alts + idents ICPattern rhs.rhs_locals)
	idents ICPattern _ = abort "idents Rhs must be called as ICExpression\n"

instance idents LocalDefs
where
	idents c (LocalParsedDefs defs) = idents c defs
	idents _ _ = abort "idents LocalDefs\n"

instance idents OptGuardedAlts
where
	idents ICExpression alts = case alts of
		UnGuardedExpr e -> idents ICExpression e
		GuardedAlts es oth -> idents ICExpression es + idents ICExpression oth
	idents ICPattern _ = abort "idents OptGuardedAlts must be called as ICExpression\n"

instance idents ExprWithLocalDefs
where
	idents ICExpression e = noLocals $
		idents ICExpression e.ewl_locals +
		idents ICExpression e.ewl_expr +
		idents ICExpression e.ewl_nodes
	idents ICPattern _ = abort "idents ExprWithLocalDefs must be called as ICExpression\n"

instance idents NodeDefWithLocals
where
	idents c ndwl = idents ICPattern ndwl.ndwl_def.bind_dst +
		noLocals (idents ICExpression ndwl.ndwl_def.bind_src + idents c ndwl.ndwl_locals)

instance idents GuardedExpr
where
	idents ICExpression e =
		idents ICExpression e.alt_guard +
		idents ICExpression e.alt_expr +
		idents ICPattern    e.alt_nodes
	idents ICPattern _ = abort "idents GuardedExpr must be called as ICExpression\n"

instance idents CaseAlt
where
	idents ICExpression a = noLocals (idents ICPattern a.calt_pattern + idents ICExpression a.calt_rhs)
	idents ICPattern _ = abort "idents CaseAlt must be called as ICExpression\n"

instance idents Qualifier
where
	idents ICPattern q =
		idents ICPattern q.qual_generators +
		idents ICPattern q.qual_let_defs +
		idents ICExpression q.qual_filter
	idents ICExpression _ = abort "idents Qualifier must be called as ICPattern\n"

instance idents FieldNameOrQualifiedFieldName
where
	idents _ (FieldName id) = {zero & record_fields=fromList [id.id_name]}
	idents _ (QualifiedFieldName _ f) = {zero & record_fields=fromList [f]}

instance idents Generator
where
	idents ICPattern g = idents ICPattern g.gen_pattern + idents ICExpression g.gen_expr
	idents ICExpression _ = abort "idents Generator must be called as ICPattern\n"

instance idents Sequence
where
	idents ICExpression s = case s of
		SQ_FromThen   _ a b   -> idents ICExpression [a,b]
		SQ_FromThenTo _ a b c -> idents ICExpression [a,b]
		SQ_From       _ a     -> idents ICExpression a
		SQ_FromTo     _ a b   -> idents ICExpression [a,b]
	idents ICPattern _ = abort "idents Sequence must be called as ICExpression\n"

instance idents ParsedSelection
where
	idents c ps = case ps of
		PS_Record id _           -> {zero & record_fields=fromList [id.id_name]}
		PS_QualifiedRecord _ s _ -> {zero & record_fields=fromList [s]}
		PS_Array e               -> idents c e
		PS_Erroneous             -> zero

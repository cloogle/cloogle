definition module Clean.Idents

/**
 * Functions to find identifiers in Clean source code.
 *
 * Copyright 2016-2023 the authors (see README.md).
 *
 * This file is part of Cloogle.
 *
 * Cloogle is free software: you can redistribute it and/or modify it under the
 * terms of the GNU Affero General Public License as published by the Free
 * Software Foundation, version 3 of the License.
 *
 * Cloogle is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE.  See the GNU Affero General Public License for
 * more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with Cloogle. If not, see <https://www.gnu.org/licenses/>.
 *
 * The software is licensed under additional terms under section 7 of the GNU
 * Affero General Public License; see the LICENSE file for details.
 */

from StdClass import class Eq
from StdList import isMember, removeDup
from StdOverloaded import class ==

from Data.Set import :: Set

from syntax import :: ParsedDefinition

:: Idents =
	{ locals        :: !Set String
	, globals       :: !Set String
	, record_fields :: !Set String
	}

:: IdentContext
	= ICExpression
	| ICPattern

class idents t :: !IdentContext !t -> Idents

instance idents [t] | idents t
instance idents ParsedDefinition

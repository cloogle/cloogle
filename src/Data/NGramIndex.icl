implementation module Data.NGramIndex

/**
 * Copyright 2016-2023 the authors (see README.md).
 *
 * This file is part of Cloogle.
 *
 * Cloogle is free software: you can redistribute it and/or modify it under the
 * terms of the GNU Affero General Public License as published by the Free
 * Software Foundation, version 3 of the License.
 *
 * Cloogle is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE.  See the GNU Affero General Public License for
 * more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with Cloogle. If not, see <https://www.gnu.org/licenses/>.
 *
 * The software is licensed under additional terms under section 7 of the GNU
 * Affero General Public License; see the LICENSE file for details.
 */

import _SystemArray
import StdChar
from StdFunc import flip, o
import StdInt
from StdList import filter, flatten, isMember, map, removeDup, span, take, ++,
	instance length [], instance == [a], instance < [a],
	instance fromString [a], instance toString [a], foldr
import StdOrdList
import StdString

from Data.Func import $
from Data.List import concatMap, tails
import Data.Map
import Data.Maybe
import Data.Monoid

newNGramIndex :: !Int !Bool -> NGramIndex v
newNGramIndex n ci = {n=n, ci=ci, idx=newMap}

ngramSize :: !(NGramIndex v) -> Int
ngramSize ngi = mapSize ngi.idx

index :: !String !v !(NGramIndex v) -> NGramIndex v | Eq v
index s v ngi=:{n,ci,idx} = {ngi & idx=foldr add idx (ngrams` ci n s)}
where
	add = alter \vs -> ?Just case vs of
		?None    -> [v]
		?Just vs -> if (isMember v vs) vs [v:vs]
	ngrams` ci n s = flatten [ngrams ci i s \\ i <- [1..n]]

search :: !String !(NGramIndex v) -> [(v,Int)] | Eq, Ord v
search s {n,ci,idx} = count
	$ foldr merge []
	$ map (fromMaybe [] o flip get idx)
	$ if (size s >= n) (ngrams ci n s) [{toLower c \\ c <-: s}]
where
	count :: [v] -> [(v,Int)] | == v
	count [] = []
	count [x:xs] = [(x,length yes + 1):count no]
	where
		(yes,no) = span ((==) x) xs

ngrams :: !Bool !Int !String -> [String]
ngrams ci n s = removeDup $ loop (size s - n) s
where
	loop :: !Int !String -> [String]
	loop  i s
	| i < 0     = []
	| otherwise = [if ci {toLower c \\ c <-: ngram} ngram:loop (i-1) s]
	where
		ngram = s % (i,i+n-1)
